﻿using System;
using System.Globalization;

namespace Saodat.Helpers
{
    public static class FormatPrice
    {
        public static string Format(decimal price)
        {

            if (price < 100)
            {
                return price.ToString(CultureInfo.InvariantCulture);
            }

            var nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = " ";
            string formatted = price.ToString("#,00", nfi);
            return formatted;
        }
    }
}