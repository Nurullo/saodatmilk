﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Saodat.Data.Entities;
using System.Data.Entity;

namespace Saodat.Data.Repository
{
    public interface IRepository<T> where T : class, IEntity
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Add(T entity);
        void Update(T entity);
        //void Update(List<ReturnProduct> list);

        void Delete(int id);
        DbSet<T> GetDbSet();
        

        IQueryable<T> All();

        Task<bool> ExistsAsync(int id);
        bool Exists(int id);
     
        bool Any(Expression<Func<T, bool>> predicate);
        Task<bool> AnyAsync(Expression<Func<T, bool>> predicate);
        void Commit();

    }
}