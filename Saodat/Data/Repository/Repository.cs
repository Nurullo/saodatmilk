﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Saodat.Data.Entities;
using System.Data.Entity;

namespace Saodat.Data.Repository
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        public Repository(DbContext dbContext)
        {
            DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            DbSet = DbContext.Set<T>();
        }

        protected DbContext DbContext { get; set; }
        protected DbSet<T> DbSet { get; set; }
        public virtual T GetById(int id)
        {
            return DbSet.Find(id);
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public void Add(T entity)
        {
            DbSet.Add(entity);
            Commit();
        }

        public virtual void Update(T entity)
        {
            if (entity.Id != 0)
            {
                var databaseEntity = GetById(entity.Id);
                DbContext.Entry(databaseEntity).CurrentValues.SetValues(entity);
                Commit();
            }

        }


        public virtual void Reload(T entity)
        {
            DbContext.Entry<T>(entity).Reload();
        }

        public virtual void Delete(int id)
        {
            var entity = GetById(id);
            if (entity == null) return; // not found; assume already deleted.

        }

        public async Task<bool> ExistsAsync(int id)
        {
            return await DbSet.FindAsync(id) != null;
        }

        public bool Exists(int id)
        {
            return ExistsAsync(id).Result;
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Any(predicate);
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> predicate)
        {
            return await DbSet.AnyAsync(predicate);
        }

        public DbSet<T> GetDbSet()
        {
            return DbSet;
        }





        IQueryable<T> IRepository<T>.All()
        {

            //if (typeof(T).GetInterfaces().Contains(typeof(IDeletable)))
            //{
            //    return DbSet.AsQueryable<T>().Where(_ => (( IDeletable)_).IsDeleted == false).Cast<T>();
            //}
            return DbSet.AsQueryable<T>();

        }

        public IEnumerable<T> GetAll()
        {
            return DbSet;
        }

        IQueryable<T> IRepository<T>.GetAll()
        {
            return DbSet.AsQueryable<T>();
        }

        public void Commit()
        {
            DbContext.SaveChanges();
        }

        
    }
}