﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Data.Entities
{
    public class ReturnProduct : Modelbase
    {
        public int ProductId { get; set; }
        public int ReturnedAmount { get; set; }
        public decimal CurrentPrice { get; set; }
        public int GivenAmount { get; set; }
        public int ConsumerId { get; set; }
        public DateTime CreatedAt { get; set; }
        public virtual Product Product { get; set; }
        public virtual Expeditor Consumer { get; set; }

    }
}
