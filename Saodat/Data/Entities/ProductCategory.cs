﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Data.Entities
{
    public class ProductCategory:Modelbase
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedAt { get; set; }
    }
}
