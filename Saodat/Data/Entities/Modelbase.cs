﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Saodat.Data.Entities
{
    public abstract class Modelbase : IEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
