﻿using System;

namespace Saodat.Data.Entities
{
    public class Tare : Modelbase
    {

        public int TareTypeId { get; set; }
        public int Amount { get; set; }
        public int ExpeditorId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public TareType TareType { get; set; }
        public Expeditor Expeditor { get; set; }

    }
}