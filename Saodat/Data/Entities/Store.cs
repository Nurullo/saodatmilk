﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Data.Entities
{
    public class Store : Modelbase
    {
        [Required]
        [MaxLength(60)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Address { get; set; }
        [Required]
        [MaxLength(80)]
        public string ManagerName { get; set; }
        [Required]
        [MaxLength(16)]
        public string PhoneNumber { get; set; }
        public int ExpeditorId { get; set; }
        public int StoreTypeId { get; set; }
        public Expeditor Expeditor { get; set; }
        public StoreType StoreType { get; set; }
        public bool IsActive { get; internal set; }
    }

    
}
