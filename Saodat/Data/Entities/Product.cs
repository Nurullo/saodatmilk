﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Data.Entities
{
    public class Product : Modelbase
    {


        [Required]
        [MaxLength(60)]
        public string Name { get; set; }
        [Required]
        public int UnitId { get; set; }
        [Required]
        public decimal Price { get; set; }
        public decimal Fatness { get; set; }
        public DateTime CreatedAt { get; set; }
        public int ProductCategoryId { get; set; }
        public bool Enabled { get; set; }
        public decimal Value { get; set; }
        public virtual ICollection<ReturnProduct> ReturnRegisters { get; set; }
        public Unit Unit { get; set; }
        public ProductCategory ProductCategory { get; set; }

    }
}
