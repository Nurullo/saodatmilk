﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Saodat.Data.Entities
{
    public class TareType: Modelbase
    {
        
        [Required]
        [MaxLength(60)]
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }

        public ICollection<Tare> Tares { get; set; }
    }
}