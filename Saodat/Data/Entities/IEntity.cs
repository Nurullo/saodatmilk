﻿using System.ComponentModel.DataAnnotations;

namespace Saodat.Data.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
    
}