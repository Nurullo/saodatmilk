﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper.Configuration.Annotations;

namespace Saodat.Data.Entities
{
    public class Expeditor : Modelbase
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        [MaxLength(50)]
        public string Surname { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }
        [Required]
        [MaxLength(50)]
        public string Phone { get; set; }
      
        [MaxLength(250)]
        public string Image { get; set; }
        [Required]
        [MaxLength(100)]
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public Gender GenderId { get; set; }
        public ICollection<Store> Stores { get; set; }
    }

    public enum Gender
    {
        Male =1,
        Female
    }

}
