﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Data.Entities
{
    public class StoreType : Modelbase
    {
        [Required]
        [MaxLength(80)]
        public string Title { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
