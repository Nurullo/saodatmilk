﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace Saodat.Data.Entities
{
    public class SaodatDbContext : DbContext
    {
        public SaodatDbContext() : base("name=SaodatConnectionString") {}
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Expeditor> Expeditors { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<ReturnProduct> ReturnProducts { get; set; }
        public virtual DbSet<TareType> TareTypes  { get; set; }
        public virtual DbSet<Tare> Tares  { get; set; }
        public virtual DbSet<StoreType> StoreTypes { get; set; }

    }
}
