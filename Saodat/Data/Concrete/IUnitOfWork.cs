﻿using System;
using System.Threading.Tasks;
using Saodat.Data.Entities;
using Saodat.Data.Repository;

namespace Saodat.Data.Concrete
{
    /// <summary>
    /// Interface for the "Unit of Work"
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Save pending changes to the database
        /// </summary>
        int Commit();
        // Save pending changes async to the data store.
        Task<int> CommitAsync();
        // Repositories

        IRepository<Product> Products { get; }
        IRepository<TareType> TareTypes { get; }
        IRepository<Expeditor> Expeditors { get; }
        IRepository<ProductCategory> ProductCategories { get; }
        IRepository<Store> Stores { get; }
        IRepository<Unit> Units { get; }
        IRepository<Tare> Tares { get; }
        IRepository<ReturnProduct>  ReturnProducts { get; }
        IRepository<StoreType>  StoreTypes { get; }


    }
}