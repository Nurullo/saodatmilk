﻿using System;
using System.Threading.Tasks;
using Saodat.Data.Entities;
using Saodat.Data.Repository;

namespace Saodat.Data.Concrete
{
    /// <summary>
    /// The "Unit of Work"
    ///     1) decouples the repos from the controllers
    ///     2) decouples the DbContext and EF from the controllers
    ///     3) manages the UoW
    /// </summary>
    /// <remarks>
    /// This class implements the "Unit of Work" pattern in which
    /// the "UoW" serves as a facade for querying and saving to the database.
    /// Querying is delegated to "repositories".
    /// Each repository serves as a container dedicated to a particular
    /// root entity type such as a <see cref="System.Security.Policy.Url"/>.
    /// A repository typically exposes "Get" methods for querying and
    /// will offer add, update, and delete methods if those features are supported.
    /// The repositories rely on their parent UoW to provide the interface to the
    /// data layer (which is the EF DbContext in this example).
    /// </remarks>
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly SaodatDbContext _dbContext;
        public bool isCreated = false;
        public UnitOfWork()
        {
            //DbContext = dbContext;
            _dbContext = new SaodatDbContext();
            if (!isCreated)
            {
                Create();
                isCreated = !isCreated;
            }
        }



        // repositories
        public IRepository<Product> Products { get; private set; }
        public IRepository<Expeditor> Expeditors { get; private set; }
        public IRepository<ProductCategory> ProductCategories { get; private set; }
        public IRepository<Store> Stores { get; private set; }
        public IRepository<Unit> Units { get; private set; }
        public IRepository<ReturnProduct> ReturnProducts { get; private set; }
        public IRepository<Tare> Tares { get; private set; }
        public IRepository<TareType> TareTypes { get; private set; }

        public IRepository<StoreType> StoreTypes { get; private set; }

        public void Create()
        {
            Products = new Repository<Product>(_dbContext);
            Expeditors = new Repository<Expeditor>(_dbContext);
            ProductCategories = new Repository<ProductCategory>(_dbContext);
            Stores = new Repository<Store>(_dbContext);
            Units = new Repository<Unit>(_dbContext);
            ReturnProducts = new Repository<ReturnProduct>(_dbContext);
            Tares = new Repository<Tare>(_dbContext);
            TareTypes = new Repository<TareType>(_dbContext);
            StoreTypes = new Repository<StoreType>(_dbContext);
        }



        /// <summary>
        /// Save pending changes to the database
        /// </summary>
        public async Task<int> CommitAsync()
        {

            //System.Diagnostics.Debug.WriteLine("Committed");
            //try
            // {
            return await _dbContext.SaveChangesAsync();
            // }
            //catch (Exception ex) // DbEntityValidationException ex)
            //{
            //	Debug.WriteLine(" ");
            //foreach (var error in ex.EntityValidationErrors)
            //{
            //	Debug.WriteLine("====================");
            //	Debug.WriteLine("Entity {0} in state {1} has validation errors:",
            //		error.Entry.Entity.GetType().Name, error.Entry.State);
            //	foreach (var ve in error.ValidationErrors)
            //	{
            //		Debug.WriteLine("\tProperty: {0}, Error: {1}",
            //			ve.PropertyName, ve.ErrorMessage);
            //	}
            //	Debug.WriteLine(" ");
            //}
            // throw;
            //}
        }



        /// <summary>
        /// Save pending changes to the database
        /// </summary>
        public int Commit()
        {

            //System.Diagnostics.Debug.WriteLine("Committed");
            //try
            // {
            return _dbContext.SaveChanges();
            // }
            //catch (Exception ex) // DbEntityValidationException ex)
            //{
            //	Debug.WriteLine(" ");
            //foreach (var error in ex.EntityValidationErrors)
            //{
            //	Debug.WriteLine("====================");
            //	Debug.WriteLine("Entity {0} in state {1} has validation errors:",
            //		error.Entry.Entity.GetType().Name, error.Entry.State);
            //	foreach (var ve in error.ValidationErrors)
            //	{
            //		Debug.WriteLine("\tProperty: {0}, Error: {1}",
            //			ve.PropertyName, ve.ErrorMessage);
            //	}
            //	Debug.WriteLine(" ");
            //}
            // throw;
            //}
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext?.Dispose();
            }
        }
        #endregion
    }
}