﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Models
{
    public class TopProductModel
    {
        public int Id { get; set; }
        public int  Order { get; set; }
        public string   Name { get; set; }
        public string Returned { get; set; }
    }
}
