﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Models
{
    public class TareList: INotifyPropertyChanged
    {

        public int Order { get; set; }
        public int Id { get; set; }
        public int TareTypeId { get; set; }
        public string Title { get; set; }
        public int Amount { get; set; }
        public int ExpeditorId { get; set; }
        public DateTime CreatedAt { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        // The calling member's name will be used as the parameter.
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

    }
}
