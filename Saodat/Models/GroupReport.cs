﻿using System.Collections.Generic;

namespace Saodat.Models
{
    public class GroupReport
    {
        public List<RegisterList> Data { get; set; }
        public string Name { get; set; }
    }
}
