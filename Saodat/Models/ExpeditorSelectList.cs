﻿using System.Windows.Controls.Primitives;

namespace Saodat.Models
{
    public class ExpeditorSelectList
    {
        public int Order { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }

    }
}