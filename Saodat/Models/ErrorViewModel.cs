﻿namespace Saodat.Models
{
    public class ErrorViewModel
    {
        public int Order { get; set; }
        public string Element { get; set; }
        public string Description { get; set; }
    }
}