﻿namespace Saodat.Models
{
    public class StoreModel
    {
        public int Id { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ManagerName { get; set; }
        public string PhoneNumber { get; set; }
        public string Expeditor { get; set; }
        public string StoreType { get; set; }
        public bool IsActive { get; set; }
    }
}