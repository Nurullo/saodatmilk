﻿namespace Saodat.Models
{
    public class ProductCategoryModel
    {

        public int Order { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
    }
}
