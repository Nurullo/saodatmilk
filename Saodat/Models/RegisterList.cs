﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Saodat.Models
{
    public class RegisterList : INotifyPropertyChanged
    {
        private int givenAmount;

        public int Order { get; set; }
        public int ProductId { get; set; }
        public string Title { get; set; }
        public int ReturnedAmount { get; set; }
        public int GivenAmount
        {
            get
            {

                return givenAmount;
            }
            set
            {
                if (value <= ReturnedAmount && value >= 0)
                {
                    givenAmount = value;
                }
                else givenAmount = 0;
                OnPropertyChanged(nameof(GivenAmount));
            }
        }
        public int RegisterId { get; internal set; }

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event
        // The calling member's name will be used as the parameter.
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}

