﻿namespace Saodat.Models
{
    public class StoreTypeModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}