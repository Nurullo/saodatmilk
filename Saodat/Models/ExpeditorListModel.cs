﻿namespace Saodat.Models
{
    public class ExpeditorListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}