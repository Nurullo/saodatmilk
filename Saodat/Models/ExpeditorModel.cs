﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Saodat.Data.Entities;

namespace Saodat.Models
{
    public class ExpeditorModel : INotifyPropertyChanged
    {
        public int Order { get; set; }
        public int Id { get; set; }
        public string FullName { get; set; }
        public string BirthDate { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        private string _image;

        public string Image
        {
            get { return _image; }
            set
            {
                _image = value;
                OnPropertyChanged(nameof(Image));
            }
        }

        public string Address { get; set; }
        public bool IsActive { get; set; }
        public string Gender { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}