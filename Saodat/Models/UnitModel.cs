﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Models
{
    public class UnitModel
    {
        public int Order { get; set; }
        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsActive { get;  set; }
    }
}
