﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Models
{
    public class ReportListModel
    {
        public int Order { get; set; }
        public string Name { get; set; }
        public int Returned { get; set; }
        public int Given { get; set; }
    }
}
