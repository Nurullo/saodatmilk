﻿namespace Saodat.Models
{
    public class GenderViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}