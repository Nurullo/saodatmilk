﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.Models
{
    public class ProductModel
    {
        public int Order { get; set; }
        public int Id { get; set; }
        public string FullTitle { get; set; }
        public string Price { get; set; }
        public bool Enabled { get; set; }
        public string Unit { get; internal set; }
    }
}
