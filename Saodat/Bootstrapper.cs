﻿using Caliburn.Micro;
using Saodat.Data.Repository;
using Saodat.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using AutoMapper;
using MaterialDesignThemes.Wpf;
using OfficeOpenXml;
using Saodat.Data.Concrete;
using Saodat.Services;
using Saodat.Data.Entities;
using Saodat.Models;

namespace Saodat
{
    public class Bootstrapper : BootstrapperBase
    {
        private readonly SimpleContainer _container = new SimpleContainer();

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            _container.Instance(_container);

            _container
                .Singleton<IWindowManager, WindowManager>()
                .Singleton<IEventAggregator, EventAggregator>()
                .Singleton<DbContext, DbContext>()
                .Singleton<IUnitOfWork, UnitOfWork>()
                .Singleton<ExcelPackage>()
                .Singleton<IProductReportService,ProductReportService>();

            _container.RegisterSingleton(typeof(IRepository<>), "repo", typeof(Repository<>));
            
            MapperConfiguration config = new MapperConfiguration(cfg =>
                        {
                            cfg.CreateMap<StoreType, StoreTypeModel>(); 
                //
            });

            _container.RegisterInstance(
       typeof(IMapper),
       "automapper",
       config.CreateMapper()
   );

            GetType().Assembly.GetTypes()
                .Where(type => type.IsClass)
                .Where(type => type.Name.EndsWith("ViewModel"))
                .ToList()
                .ForEach(viewModelType => _container.RegisterPerRequest(
                    viewModelType, viewModelType.ToString(), viewModelType));

            //GetType().Assembly.GetTypes()
            //    .Where(type => type.IsClass)
            //    .Where(type => type.Name.EndsWith("Repository"))
            //    .ToList()
            //    .ForEach(rep => _container.RegisterInstance(
            //        rep, rep.ToString(), rep));

        }
        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }
    }
}
