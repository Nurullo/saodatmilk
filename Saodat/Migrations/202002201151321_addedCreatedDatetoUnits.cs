﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCreatedDatetoUnits : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Units", "CreatedAt", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Units", "CreatedAt");
        }
    }
}
