﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "ProductGroupId", "dbo.ProductCategories");
            DropIndex("dbo.Products", new[] { "ProductGroupId" });
            AddColumn("dbo.Products", "ProductCategory_Id", c => c.Int());
            CreateIndex("dbo.Products", "ProductCategory_Id");
            AddForeignKey("dbo.Products", "ProductCategory_Id", "dbo.ProductCategories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "ProductCategory_Id", "dbo.ProductCategories");
            DropIndex("dbo.Products", new[] { "ProductCategory_Id" });
            DropColumn("dbo.Products", "ProductCategory_Id");
            CreateIndex("dbo.Products", "ProductGroupId");
            AddForeignKey("dbo.Products", "ProductGroupId", "dbo.ProductCategories", "Id", cascadeDelete: true);
        }
    }
}
