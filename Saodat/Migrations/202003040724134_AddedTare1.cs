﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTare1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tares", "ExpeditorId", c => c.Int(nullable: false));
            CreateIndex("dbo.Tares", "ExpeditorId");
            AddForeignKey("dbo.Tares", "ExpeditorId", "dbo.Expeditors", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tares", "ExpeditorId", "dbo.Expeditors");
            DropIndex("dbo.Tares", new[] { "ExpeditorId" });
            DropColumn("dbo.Tares", "ExpeditorId");
        }
    }
}
