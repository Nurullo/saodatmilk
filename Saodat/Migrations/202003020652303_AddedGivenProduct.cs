﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGivenProduct : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GivenProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        ExpeditorId = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Expeditors", t => t.ExpeditorId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.ExpeditorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GivenProducts", "ProductId", "dbo.Products");
            DropForeignKey("dbo.GivenProducts", "ExpeditorId", "dbo.Expeditors");
            DropIndex("dbo.GivenProducts", new[] { "ExpeditorId" });
            DropIndex("dbo.GivenProducts", new[] { "ProductId" });
            DropTable("dbo.GivenProducts");
        }
    }
}
