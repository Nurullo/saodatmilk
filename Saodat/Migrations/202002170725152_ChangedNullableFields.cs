﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedNullableFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Value", c => c.Decimal(precision: 18, scale: 2));
        }
    }
}
