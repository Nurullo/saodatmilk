﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Expeditors", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Expeditors", "IsActive");
        }
    }
}
