﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTare : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tares",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TareTypeId = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TareTypes", t => t.TareTypeId, cascadeDelete: true)
                .Index(t => t.TareTypeId);
            
            CreateTable(
                "dbo.TareTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tares", "TareTypeId", "dbo.TareTypes");
            DropIndex("dbo.Tares", new[] { "TareTypeId" });
            DropTable("dbo.TareTypes");
            DropTable("dbo.Tares");
        }
    }
}
