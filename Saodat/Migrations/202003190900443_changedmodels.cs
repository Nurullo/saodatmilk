﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedmodels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tares", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.TareTypes", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.TareTypes", "CreatedAt", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TareTypes", "CreatedAt");
            DropColumn("dbo.TareTypes", "IsActive");
            DropColumn("dbo.Tares", "isActive");
        }
    }
}
