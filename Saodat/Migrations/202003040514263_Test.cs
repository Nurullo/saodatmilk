﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.GivenProducts", "ExpeditorId", "dbo.Expeditors");
            DropForeignKey("dbo.GivenProducts", "ProductId", "dbo.Products");
            DropIndex("dbo.GivenProducts", new[] { "ProductId" });
            DropIndex("dbo.GivenProducts", new[] { "ExpeditorId" });
            DropTable("dbo.GivenProducts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.GivenProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        ExpeditorId = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.GivenProducts", "ExpeditorId");
            CreateIndex("dbo.GivenProducts", "ProductId");
            AddForeignKey("dbo.GivenProducts", "ProductId", "dbo.Products", "Id", cascadeDelete: true);
            AddForeignKey("dbo.GivenProducts", "ExpeditorId", "dbo.Expeditors", "Id", cascadeDelete: true);
        }
    }
}
