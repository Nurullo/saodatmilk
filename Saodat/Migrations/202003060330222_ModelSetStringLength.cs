﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModelSetStringLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Expeditors", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Expeditors", "Surname", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Expeditors", "LastName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Expeditors", "Email", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Expeditors", "Phone", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Expeditors", "Image", c => c.String(nullable: true, maxLength: 250));
            AlterColumn("dbo.Expeditors", "Address", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Stores", "Name", c => c.String(nullable: false, maxLength: 60));
            AlterColumn("dbo.Stores", "Address", c => c.String(maxLength: 100));
            AlterColumn("dbo.Stores", "ManagerName", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.Stores", "PhoneNumber", c => c.String(nullable: false, maxLength: 16));
            AlterColumn("dbo.ProductCategories", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Products", "Name", c => c.String(nullable: false, maxLength: 60));
            AlterColumn("dbo.Units", "Name", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.TareTypes", "Name", c => c.String(nullable: false, maxLength: 60));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TareTypes", "Name", c => c.String());
            AlterColumn("dbo.Units", "Name", c => c.String());
            AlterColumn("dbo.Products", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.ProductCategories", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Stores", "PhoneNumber", c => c.String());
            AlterColumn("dbo.Stores", "ManagerName", c => c.String());
            AlterColumn("dbo.Stores", "Address", c => c.String());
            AlterColumn("dbo.Stores", "Name", c => c.String());
            AlterColumn("dbo.Expeditors", "Address", c => c.String());
            AlterColumn("dbo.Expeditors", "Image", c => c.String());
            AlterColumn("dbo.Expeditors", "Phone", c => c.String());
            AlterColumn("dbo.Expeditors", "Email", c => c.String());
            AlterColumn("dbo.Expeditors", "LastName", c => c.String());
            AlterColumn("dbo.Expeditors", "Surname", c => c.String());
            AlterColumn("dbo.Expeditors", "Name", c => c.String());
        }
    }
}
