﻿namespace Saodat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StoreTypeEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StoreTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 80),
                        IsActive = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Stores", "StoreTypeId");
            AddForeignKey("dbo.Stores", "StoreTypeId", "dbo.StoreTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Stores", "StoreTypeId", "dbo.StoreTypes");
            DropIndex("dbo.Stores", new[] { "StoreTypeId" });
            DropTable("dbo.StoreTypes");
        }
    }
}
