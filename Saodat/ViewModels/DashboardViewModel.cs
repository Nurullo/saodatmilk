﻿using Caliburn.Micro;
using Saodat.Data.Concrete;
using Saodat.Models;
using Saodat.Views.Register;
using System;
using System.Data.Entity;
using System.Linq;

namespace Saodat.ViewModels
{
    public class DashboardViewModel : Screen
    {
        private readonly IEventAggregator _events;
        private readonly IUnitOfWork _unitOfWork;

        public DashboardViewModel(IEventAggregator events, IUnitOfWork unitOfWork)
        {
            _events = events;
            _unitOfWork = unitOfWork;
            GetData();
        }

        private void GetData()
        {
            var today = DateTime.Now.Date;
            var week = today.AddDays(-7);
            var query = (from p in _unitOfWork.Products.GetDbSet().Include(x => x.Unit).Where(x => x.Enabled == true).ToList()
                         join rp in _unitOfWork.ReturnProducts.All().Where(x => DbFunctions.TruncateTime(x.CreatedAt) >= week && DbFunctions.TruncateTime(x.CreatedAt) <= today).ToList() on p.Id equals rp.ProductId into list
                         from ll in list.GroupBy(x => x.ProductId).DefaultIfEmpty()
                         select new TopProductModel
                         {
                             Id = p.Id,
                             Name = $"{p.Name} {p.Value.ToString("G29")}{p.Unit.Name} {p.Fatness.ToString("G29")}%",
                             Returned = ll.Sum(x => x.ReturnedAmount).ToString()
                         }).ToList();
            int cnt = 1;
            for (int i = 0; i < query.Count; i++)
            {
                query[i].Order = cnt;
                cnt++;
            }
            Data = new BindableCollection<TopProductModel>(query);

            Products = _unitOfWork.Products.All().Count(x=>x.Enabled == true);
            Expeditors = _unitOfWork.Expeditors.All().Count(x=>x.IsActive == true);
            Stores = _unitOfWork.Stores.All().Count(x=>x.IsActive == true);
            Categories = _unitOfWork.ProductCategories.All().Count(x=>x.IsActive== true);
        }

        public int Products { get; set; }
        public int Stores { get; set; }
        public int Expeditors { get; set; }
        public int Categories { get; set; }
        
        public IObservableCollection<TopProductModel> Data { get; set; }

        #region Properties

        #endregion

        public void Register()
        {
            _events.PublishOnUIThread("Register");
        }
    }
}