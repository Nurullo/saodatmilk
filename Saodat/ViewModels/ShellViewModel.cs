﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Threading;
using Caliburn.Micro;
using MaterialDesignThemes.Wpf;
using Saodat.Models;
using Saodat.ViewModels.Expeditor;
using Saodat.ViewModels.Product;
using Saodat.ViewModels.ProductCategory;
using Saodat.ViewModels.Register;
using Saodat.ViewModels.Report;
using Saodat.ViewModels.Store;
using Saodat.ViewModels.Unit;

namespace Saodat.ViewModels
{
    public class ShellViewModel : Conductor<object>, IHandle<string>
    {
        private readonly IWindowManager _windowManager;
        private IEventAggregator _events;
        private SimpleContainer _container;
        private bool _isOpenDialog = false;
     

        public ShellViewModel(IEventAggregator events, SimpleContainer container, IWindowManager windowManager)
        {
            _windowManager = windowManager;
            _events = events;
            _container = container;
            _events.Subscribe(this);

            ActivateItem(_container.GetInstance<DashboardViewModel>());

            DispatcherTimer liveTime = new DispatcherTimer();
            liveTime.Interval = TimeSpan.FromSeconds(1);
            liveTime.Tick += LiveTime_Tick;
            liveTime.Start();
        }

        private void LiveTime_Tick(object sender, EventArgs e)
        {
            Date = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
        }

        private string _date;

        public string Date
        {
            get { return _date; }
            set
            {
                _date = value;
                NotifyOfPropertyChange(() => Date);
            }
        }

        public void Schedule()
        {
            //ActivateItem(_container.GetInstance<DashboardViewModel>());

        }

        public void ProductCategory()
        {
            ActivateItem<ProductCategoryCollectionViewModel>();
        }
        public void Unit()
        {
            ActivateItem<UnitCollectionViewModel>();
        }
        public void Expeditor()
        {
            ActivateItem<ExpeditorCollectionViewModel>();
        }
        public void Store()
        {
            ActivateItem<StoreCollectionViewModel>();
        }

        public void Dashboard()
        {
            ActivateItem<DashboardViewModel>();
        }
        public void Report()
        {
            ActivateItem<ReportViewModel>();
        }


        public void Test()
        {
            ActivateItem<ProductCollectionViewModel>();
        }

        public void Exit()
        {
            this.TryClose();
        }

        public void Register()
        {
            ActivateItem<RegisterCollectionViewModel>();
        }


        public void ActivateItem<T>()
        {
            ActivateItem(_container.GetInstance<T>());
        }
        public void Handle(string message)
        {
            switch (message)
            {
                case "Product":
                    {

                        // DialogCrud = _container.GetInstance<ProductViewModel>();
                        // ActivateItem(DialogCrud);
                    }
                    break;
                case "Register":
                    {
                        ActivateItem<RegisterCollectionViewModel>();
                    }
                    break;
            }
        }
    }
}
