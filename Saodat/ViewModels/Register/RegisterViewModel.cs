﻿using Caliburn.Micro;
using Saodat.Data.Concrete;
using Saodat.Data.Entities;
using Saodat.Models;
using Saodat.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;

namespace Saodat.ViewModels.Register
{
    public class RegisterViewModel : ScreenBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public int Order = 1;
        public int ExpeditorId = 0;
        public RegisterViewModel(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #region Properties
        private ObservableCollection<TareList> _data;
        public ObservableCollection<TareList> Data
        {
            get => _data;
            set
            {
                _data = value;
                NotifyOfPropertyChange(() => Data);
            }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (value != " ")
                {
                    _name = value; NotifyOfPropertyChange(() => Name);

                }
            }
        }
        private DateTime _date;
        public DateTime Date
        {
            get
            {
                return _date;
            }

            set
            {
                if (value > DateTime.Now)
                {
                    SnackBarMessage.Enqueue("не выбирайте будущую дату");
                }
                else
                {
                    _date = value;
                }

                NotifyOfPropertyChange(() => Date);
                GetData();
            }
        }
        private void GetData()
        {
            Order = 1;
            var linq = (
                  from s in _unitOfWork.TareTypes.All()
                          .Where(x => x.IsActive == true)
                  join r in _unitOfWork.Tares.All()
                          .Where(x =>
                                      DbFunctions.TruncateTime(x.CreatedAt) == DbFunctions.TruncateTime(Date) &&
                                      x.ExpeditorId == ExpeditorId)
                      on s.Id equals r.TareTypeId into list
                  from ll in list.DefaultIfEmpty()
                  select new TareList
                  {
                      Amount = (ll == null ? 0 : ll.Amount),
                      CreatedAt = Date,
                      Title = s.Name,
                      TareTypeId = s.Id,
                      ExpeditorId = ExpeditorId,
                      Id = (ll == null ? 0 : ll.Id)
                  }).ToList();
            //Add items in Data with Order 
            List<TareList> listData = new List<TareList>();
            foreach (var item in linq)
            {
                item.Order = Order;
                listData.Add(item);
                Order++;
            }
            Data = new BindableCollection<TareList>(listData);
        }
        #endregion
        #region Buttons
        public void Save()
        {
            List<TareList> updatList = Data.Where(x => x.Amount != 0).ToList<TareList>();
            List<Tare> register = new List<Tare>();
            foreach (var item in updatList)
            {
                register.Add(new Tare()
                {
                    Amount = item.Amount,
                    CreatedAt = item.CreatedAt,
                    ExpeditorId = ExpeditorId,
                    Id = item.Id,
                    TareTypeId = item.TareTypeId
                });
            }

            Update(register);
            GetData();
        }
        #endregion

        private void Update(List<Tare> list)
        {
            foreach (var item in list)
            {
                if (item.Id != 0)
                {
                    _unitOfWork.Tares.Update(item);
                }
                else
                {
                    _unitOfWork.Tares.Add(item);
                }
            }
        }
    }
}