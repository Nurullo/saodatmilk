﻿using Caliburn.Micro;
using Saodat.Data.Concrete;
using Saodat.Data.Entities;
using Saodat.Models;
using Saodat.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;

namespace Saodat.ViewModels.Register
{
    public class RegisterCollectionViewModel : ConductorBase
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly SimpleContainer _container;
        public int Order = 1;
        public RegisterCollectionViewModel(IUnitOfWork unitOfWork, SimpleContainer container)
        {
            _unitOfWork = unitOfWork;
            _container = container;
            var ExpTypes = new List<GroupModel>()
            {
                new GroupModel(){Id = 1, Title = "Експедитор"},
                new GroupModel(){Id = 2, Title = "Супермаркет"}
            };

            Types = new List<GroupModel>(ExpTypes);
            SelectedType = Types[0];
            Date = DateTime.Now;

        }

        private bool _isOpenDialog = false;
        public bool IsOpenDialog
        {
            get => _isOpenDialog;
            set
            {
                _isOpenDialog = value;
                NotifyOfPropertyChange(() => IsOpenDialog);
            }
        }
        #region Properties

        private ObservableCollection<RegisterList> _data;
        public ObservableCollection<RegisterList> Data
        {
            get => _data;
            set
            {
                _data = value;
                NotifyOfPropertyChange(() => Data);
            }
        }
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; NotifyOfPropertyChange(() => Name); }
        }

        private DateTime _date;
        public DateTime Date
        {
            get
            {
                return _date;
            }

            set
            {
                if (value > DateTime.Now)
                {
                    SnackBarMessage.Enqueue("не выбирайте будущую дату");
                }
                else
                {
                    _date = value;
                }

                NotifyOfPropertyChange(() => Date);
                GetData();
            }
        }
        private List<GroupModel> _types = null;
        public List<GroupModel> Types
        {
            get => _types;
            set
            {
                _types = value;
                NotifyOfPropertyChange(() => Types);
            }
        }

        private GroupModel _selectedType;
        public GroupModel SelectedType
        {
            get => _selectedType;
            set
            {
                _selectedType = value;
                NotifyOfPropertyChange(() => SelectedType);
                GetExpList();

            }
        }

        private List<ExpeditorSelectList> _expeditors;
        public List<ExpeditorSelectList> Expeditors
        {
            get => _expeditors;
            set
            {
                _expeditors = value;
                NotifyOfPropertyChange(() => Expeditors);

            }
        }

        private ExpeditorSelectList _selectedExpeditor;
        public ExpeditorSelectList SelectedExpeditor
        {
            get => _selectedExpeditor;
            set
            {
                _selectedExpeditor = value;
                NotifyOfPropertyChange(() => SelectedExpeditor);
                if (SelectedExpeditor.Id == 1)
                {
                    var fullname = _selectedExpeditor?.Title.Split(' ');
                    Name = fullname?[0] + " " + fullname?[1].Substring(0, 1);
                }
                else
                    Name = _selectedExpeditor?.Title;
                if (_selectedExpeditor != null)
                {
                    GetData();
                }
            }
        }
        #endregion

        #region Buttons
        public void Tare()
        {
            IsOpenDialog = !IsOpenDialog;
            var update = _container.GetInstance<RegisterViewModel>();
            update.ExpeditorId = SelectedExpeditor.Id;
            update.Name = SelectedExpeditor.Title;
            update.Date = Date;
            ActivateItem(update);
        }
        public void Save()
        {
            List<RegisterList> updatList = Data.Where(x => x.ReturnedAmount != 0 || x.GivenAmount != 0).ToList<RegisterList>();
            List<ReturnProduct> register = new List<ReturnProduct>();
            foreach (var item in updatList)
            {
                register.Add(new ReturnProduct()
                {
                    Id = item.RegisterId,
                    ConsumerId = SelectedExpeditor.Id,
                    CreatedAt = Date,
                    ProductId = item.ProductId,
                    GivenAmount = item.GivenAmount,
                    ReturnedAmount = item.ReturnedAmount,
                    CurrentPrice = GetCurrentPrice(item.ProductId)
                });
            }

            Update(register);
            GetData();
        }

        private void Update(List<ReturnProduct> list)
        {
            foreach (var item in list)
            {
                if (item.Id != 0)
                {
                    _unitOfWork.ReturnProducts.Update(item);
                }
                else
                {
                    _unitOfWork.ReturnProducts.Add(item);
                }
            }

        }

        private decimal GetCurrentPrice(int id)
        {
            return _unitOfWork.Products.GetById(id).Price;
        }
        #endregion

        #region Methods

        private void GetData()
        {
            Order = 1;
            var linq = (
                  from s in _unitOfWork.Products.All()
                          .Where(x => x.Enabled == true)
                  join r in _unitOfWork.ReturnProducts.All()
                          .Where(x =>
                                      DbFunctions.TruncateTime(x.CreatedAt) == DbFunctions.TruncateTime(Date) &&
                                      x.ConsumerId == _selectedExpeditor.Id)
                      on s.Id equals r.ProductId into list
                  from ll in list.DefaultIfEmpty()
                  select new RegisterList
                  {

                      ProductId = s.Id,
                      ReturnedAmount = (ll == null ? 0 : ll.ReturnedAmount),
                      GivenAmount = (ll == null ? 0 : ll.GivenAmount),
                      RegisterId = (ll == null ? 0 : ll.Id),
                      Title = s.Name + " " + s.Fatness + " " + s.Unit.Name
                  }).ToList();

            //Add items in Data with Order 
            List<RegisterList> listData = new List<RegisterList>();
            foreach (var item in linq)
            {
                item.Order = Order;
                listData.Add(item);
                Order++;
            }
            Data = new BindableCollection<RegisterList>(listData);
        }

        private void GetExpList()
        {
            List<ExpeditorSelectList> expList;
            if (SelectedType.Id == 1)
            {
                var test = _unitOfWork.Expeditors.GetAll().Where(x => x.IsActive == true).ToList();
                expList = _unitOfWork.Expeditors.GetAll().Where(x => x.IsActive == true && x.Stores.Count(s => s.StoreTypeId == 2) == 0).Select(s => new ExpeditorSelectList()
                {
                    Id = s.Id,
                    Title = s.Surname + " " + s.Name + " " + s.LastName
                }).ToList();
            }
            else
            {
                expList = _unitOfWork.Stores.GetAll().Where(x => x.IsActive == true && x.StoreTypeId == 2).Select(s => new ExpeditorSelectList()
                {
                    Id = s.ExpeditorId,
                    Title = s.Name
                }).ToList();
            }
            Expeditors = new List<ExpeditorSelectList>(expList);
            if (Expeditors.Count > 0)
            {
                SelectedExpeditor = Expeditors[0];
            }
        }
        #endregion

    }


}