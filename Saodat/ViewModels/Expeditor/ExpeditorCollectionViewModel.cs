﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Saodat.Data.Concrete;
using Saodat.Data.Entities;
using Saodat.Models;
using Saodat.ViewModels.Base;
using Saodat.ViewModels.Product;


namespace Saodat.ViewModels.Expeditor
{
    public class ExpeditorCollectionViewModel : ConductorBase, ICollection, IHandle<bool>, IHandle<string>
    {
        private readonly IEventAggregator _events;
        private readonly SimpleContainer _container;
        private readonly IUnitOfWork _unitOfWork;
        public int order = 1;

        private ObservableCollection<ExpeditorModel> _data;
        public ObservableCollection<ExpeditorModel> Data
        {
            get => _data;
            set
            {
                _data = value;
                NotifyOfPropertyChange(() => Data);
            }
        }


        private bool _isOpenDialog = false;
        public bool IsOpenDialog
        {
            get => _isOpenDialog;
            set { _isOpenDialog = value; NotifyOfPropertyChange(() => IsOpenDialog); }
        }

        #region constructor
        public ExpeditorCollectionViewModel(IEventAggregator events, SimpleContainer container, IUnitOfWork unitOfWork)
        {
            _container = container;
            _unitOfWork = unitOfWork;
            _events = events;
            _events.Subscribe(this);
            PageSize = new List<int>() { 10, 20, 30 };
            SelectedPageSize = PageSize[0];
            GetList(_keyword);
        }
        #endregion


        #region Buttons
        public void NewExpeditor()
        {
            _events.Subscribe(this);
            //_events.PublishOnUIThread("Product");
            IsOpenDialog = !IsOpenDialog;
            ActivateItem(_container.GetInstance<ExpeditorViewModel>());
        }
        #endregion

        #region Properties

        private List<int> _pageSize;

        public List<int> PageSize
        {
            get => _pageSize;
            set
            {
                _pageSize = value;
                NotifyOfPropertyChange(() => PageSize);

            }
        }
        private int _page = 1;

        public int Page
        {
            get => _page;
            set
            {
                _page = value;
                NotifyOfPropertyChange(() => Page);
                NotifyOfPropertyChange(() => CanNextPage);
                NotifyOfPropertyChange(() => CanPreviousPage);
            }
        }

        private int _selectedPageSize = 10;

        public int SelectedPageSize
        {
            get => _selectedPageSize;
            set
            {
                _selectedPageSize = value;
                NotifyOfPropertyChange(() => SelectedPageSize);
                GetList(null);
            }
        }

        private int _pageCount;

        public int PageCount
        {
            get => _pageCount;
            set
            {
                _pageCount = value;
                NotifyOfPropertyChange(() => PageCount);

            }
        }



        //seach Keyword
        private string _keyword;
        public string Keyword
        {
            get => _keyword;
            set
            {
                _keyword = value;
                NotifyOfPropertyChange(() => Keyword);
                GetList(_keyword);
            }
        }

        //seach Keyword
        public bool _isActive = true;
        public bool IsActive
        {
            get => _isActive;
            set
            {
                _isActive = value;
                NotifyOfPropertyChange(() => IsActive);
                //if (_selecteditem != null)
                //{ //Data.Entities.Product product = new Data.Entities.Product();
                //    var exist = _unitOfWork.Products.GetById(_selecteditem.Id);
                //    if (exist != null)
                //    {
                //        exist.Enabled = _isActive;
                //        _unitOfWork.Products.Update(exist);
                //        _unitOfWork.Commit();
                //    }
                //}
            }
        }

        private ExpeditorModel _selecteditem;

        public ExpeditorModel Selecteditem
        {
            get => _selecteditem;
            set
            {
                _selecteditem = value;
                NotifyOfPropertyChange(() => Selecteditem);

            }
        }


        public bool CanNextPage
        {
            get
            {
                if (Page < _pageCount)
                {
                    return true;
                }
                return false;
            }
        }

        public bool CanPreviousPage
        {
            get
            {
                if (Page > 1)
                {
                    return true;
                }
                return false;
            }
        }

        private string _totalExpeditors;

        public string TotalExpeditor
        {
            get => _totalExpeditors;
            set
            {
                _totalExpeditors = value;
                NotifyOfPropertyChange(() => TotalExpeditor);
            }
        }


        private ExpeditorModel _leftDoubleClick;

        public ExpeditorModel LeftDoubleClick
        {
            get => _leftDoubleClick;
            set
            {
                _leftDoubleClick = value;
                NotifyOfPropertyChange(() => LeftDoubleClick);
                Edit();
            }
        }
        #endregion

        public void GetList(string keyword)
        {
            order = 1;
            List<ExpeditorModel> list = new List<ExpeditorModel>();
            var query = keyword != null ? _unitOfWork.Expeditors.All().Where(p => p.Name.Contains(keyword)).ToList() : _unitOfWork.Expeditors.All().ToList();

            //total products
            TotalExpeditor = _unitOfWork.Expeditors.All().Count().ToString();
            PageCount = (int)Math.Ceiling(query.Count() / (double)_selectedPageSize);
            // Get by page
            int skip = _selectedPageSize * (Page - 1);
            var entities = query.Skip(skip).Take(_selectedPageSize).ToList();
            foreach (var item in entities)
            {
                list.Add(new ExpeditorModel
                {    
                    Order = order,
                    Id = item.Id,
                    Address = item.Address,
                    BirthDate = item.BirthDate.ToShortDateString(),
                    Email = item.Email,
                    Gender = ((Gender)item.GenderId).ToString(),
                    Image = item.Image != null ? Directory.GetCurrentDirectory() + "\\" + Path.Combine("Images", item.Image) : null,
                    Phone = item.Phone,
                    FullName = item.Surname + " " + item.Name + " " + item.LastName,
                    IsActive = item.IsActive
                });
                order++;
            }
            Data = new BindableCollection<ExpeditorModel>(list);
        }


        public void NextPage()
        {
            Page++;
            GetList(null);
        }
        public void PreviousPage()
        {
            Page--;
            GetList(null);
        }

        public void Edit()
        {
            IsOpenDialog = !IsOpenDialog;
            var update = _container.GetInstance<ExpeditorViewModel>();
            update.ExpeditorId = Selecteditem.Id;
            Selecteditem.Image = null;
            ActivateItem(update);
        }

        public void Handle(bool message)
        {
            IsOpenDialog = message;
            GetList(null);
        }

        public void Handle(string message)
        {
            GetList(null);
        }

    }
}
