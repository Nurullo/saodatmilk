﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using Caliburn.Micro;
using Microsoft.Win32;
using Saodat.Data.Concrete;
using Saodat.Data.Entities;
using Saodat.Data.Repository;
using Saodat.Models;
using Saodat.ViewModels.Base;
using Saodat.ViewModels.Product;

namespace Saodat.ViewModels.Expeditor
{
    public class ExpeditorViewModel : ScreenBase, IViewModel
    {
        private readonly IEventAggregator _events;
        private readonly IUnitOfWork _unitOfWork;

        public ExpeditorViewModel(IEventAggregator events, IUnitOfWork unitOfWork)
        {

            _events = events;
            _unitOfWork = unitOfWork;

            var gender = new List<GenderViewModel>
            {
                new GenderViewModel() {Name = "Муж", Id = 1},
                new GenderViewModel() {Name = "Жен", Id = 2}
            };
            Gender = gender.ToList();

        }

        #region Properties

        private int _expeditorId;

        public int ExpeditorId
        {
            get { return _expeditorId; }
            set
            {
                _expeditorId = value;
                NotifyOfPropertyChange(() => ExpeditorId);
                Update(_expeditorId);
            }
        }

        private string imageName;
        private bool IsSelected = false;
        private string _image;

        public string Image
        {
            get => _image ?? "../../Images/user.png";
            set
            {
                _image = value;
                NotifyOfPropertyChange(() => Image);
            }
        }

        private List<GenderViewModel> _gender;

        public List<GenderViewModel> Gender
        {
            get { return _gender; }
            set
            {
                _gender = value;
                NotifyOfPropertyChange(() => Gender);

            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (value != " ")
                {
                    _name = value;
                }
                NotifyOfPropertyChange(() => Name);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);

            }
        }
        private string _surname;

        public string Surname
        {
            get { return _surname; }
            set
            {
                _surname = value;
                NotifyOfPropertyChange(() => Surname);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                NotifyOfPropertyChange(() => LastName);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private GenderViewModel _selectedGender;

        public GenderViewModel SelectedGender
        {
            get { return _selectedGender; }
            set
            {
                _selectedGender = value;
                NotifyOfPropertyChange(() => SelectedGender);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                NotifyOfPropertyChange(() => Email);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private string _address;

        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
                NotifyOfPropertyChange(() => Address);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }
        private string _phoneNumber;

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                _phoneNumber = value;
                NotifyOfPropertyChange(() => PhoneNumber);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private DateTime _birthDate = new DateTime(2000, 1, 1);

        public DateTime BirthDate
        {
            get => _birthDate;
            set
            {
                _birthDate = value;
                NotifyOfPropertyChange(() => BirthDate);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private bool _enabled;
        public bool Enabled
        {
            get => _enabled;
            set
            {
                _enabled = value;
                NotifyOfPropertyChange(() => Enabled);
            }
        }

        public bool CanSaveNew
        {
            get
            {
                if (ExpeditorId != 0)
                {
                    return false;
                }
                return Name != null &&
PhoneNumber != null &&
DateTime.Now.Year - BirthDate.Year >= 18 &&
Address != null &&
SelectedGender != null &&
PhoneNumber != null;
            }
        }

        public bool CanSaveClose =>
            Name != null &&
            PhoneNumber != null &&
            DateTime.Now.Year - BirthDate.Year >= 18 &&
            Address != null &&
            SelectedGender != null &&
            PhoneNumber != null;

        #endregion

        #region Buttons

        public void SelectImage()
        {
            OpenFileDialog op = new OpenFileDialog
            {
                Title = "Select a picture",
                Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                         "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                         "Portable Network Graphic (*.png)|*.png",
            };

            if (op.ShowDialog() == true)
            {
                Image = op.FileName;
                IsSelected = true;
            }

        }

        public void Cancel()
        {
            _events.PublishOnUIThread("Cancel");
        }

        public void SaveNew()
        {
            Save();
            Clear();
        }

        public void SaveClose()
        {
            Save();
            if (ExpeditorId == 0)
            {
                // tell collectionviewmodel to close dialoghost
                _events.PublishOnUIThread(false);
            }
        }

        public void Clear()
        {
            Surname = null;
            Name = null;
            LastName = null;
            Email = null;
            Address = null;
            PhoneNumber = null;
        }
        #endregion

        public void Save()
        {
            if (ExpeditorId != 0)
            {
                var update = _unitOfWork.Expeditors.GetById(ExpeditorId);
                update.Address = Address;
                update.BirthDate = BirthDate;
                update.Email = Email;
                update.GenderId = (Gender)SelectedGender.Id;
                update.LastName = LastName;
                update.IsActive = Enabled;
                update.Name = Name;
                update.Surname = Surname;
                update.Phone = PhoneNumber;
                if (IsSelected)
                {
                    _events.PublishOnUIThread("Delete");
                    if (update.Image != null)
                    {
                        File.Delete(Directory.GetCurrentDirectory() + "\\" + Path.Combine("Images", update.Image));

                    }
                    var filename = Name + Guid.NewGuid().ToString() + Path.GetExtension(Image);
                    var path = Path.Combine(Directory.GetCurrentDirectory(), Path.Combine("Images", Path.GetFileName(filename)));
                    if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\" + "Images"))
                    {
                        Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\" + "Images");
                    }
                    File.Copy(Image, path);
                    update.Image = filename;


                }
                _unitOfWork.Expeditors.Update(update);
                _unitOfWork.Commit();
                _events.PublishOnUIThread(false);
            }
            else
            {
                string fileName = null;
                if (Image != "../../Images/user.png")
                {
                    string folder = "Images";
                    fileName = Name + Guid.NewGuid().ToString() + Path.GetExtension(Image);
                    var path = Path.Combine(Directory.GetCurrentDirectory(), Path.Combine(folder, Path.GetFileName(fileName)));
                    if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\" + folder))
                    {
                        Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\" + folder);
                    }
                    File.Copy(Image, path);
                }

                Data.Entities.Expeditor product = new Data.Entities.Expeditor()
                {
                    Address = Address,
                    BirthDate = BirthDate,
                    Email = Email,
                    GenderId = (Gender)SelectedGender.Id,
                    LastName = LastName,
                    Name = Name,
                    Surname = Surname,
                    Phone = PhoneNumber,
                    Image = fileName
                };
                _unitOfWork.Expeditors.Add(product);
                _unitOfWork.Commit();

            }
        }

        public void Update(int id)
        {
            var update = _unitOfWork.Expeditors.GetById(ExpeditorId);
            Address = update.Address;
            BirthDate = update.BirthDate;
            Email = update.Email;
            SelectedGender = Gender.FirstOrDefault(x => (Gender)x.Id == (Gender)update.GenderId);
            LastName = update.LastName;
            Name = update.Name;
            Enabled = update.IsActive;
            Surname = update.Surname;
            PhoneNumber = update.Phone;
            if (update.Image != null)
            {
                Image = Directory.GetCurrentDirectory() + "\\" + Path.Combine("Images", update.Image);
            }
            Title = "Update";
        }


    }
}