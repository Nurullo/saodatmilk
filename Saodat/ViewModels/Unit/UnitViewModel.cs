﻿using System;
using System.Collections.ObjectModel;
using Caliburn.Micro;
using Saodat.Data.Repository;
using System.Linq;
using MaterialDesignThemes.Wpf;
using Saodat.Data.Concrete;
using Saodat.Models;
using Saodat.ViewModels.Base;

namespace Saodat.ViewModels.Unit
{
    public class UnitViewModel : ScreenBase, IViewModel
    {
        private readonly IEventAggregator _events;
        private readonly IUnitOfWork _unitOfWork;

        public UnitViewModel(IEventAggregator events, IUnitOfWork unitOfWork)
        {
            _events = events;
            _unitOfWork = unitOfWork;
        }
        #region Properties
        private int _unitId = 0;
        public int UnitId
        {
            get => _unitId;
            set
            {
                _unitId = value;
                NotifyOfPropertyChange(() => UnitId);
                Update(_unitId);
            }
        }
        private string _unitName;

        public string UnitName
        {
            get => _unitName;
            set
            {
                _unitName = value;
                NotifyOfPropertyChange(() => UnitName);
                NotifyOfPropertyChange(() => CanSaveNew);
                NotifyOfPropertyChange(() => CanSaveClose);
            }
        }

        private ObservableCollection<ErrorViewModel> _errors;
        public ObservableCollection<ErrorViewModel> Errors
        {
            get => _errors;
            set
            {
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }


        private bool _enabled;
        public bool Enabled
        {
            get => _enabled;
            set
            {
                _enabled = value;
                NotifyOfPropertyChange(() => Enabled);
            }
        }

        public bool CanSaveNew
        {
            get
            {
                if (UnitId!=0)
                {
                    return false;
                }
                return !string.IsNullOrWhiteSpace(UnitName);
            }
        }

        public bool CanSaveClose => !string.IsNullOrWhiteSpace(UnitName);

        #endregion
        #region Buttons


        public void Cancel()
        {

        }

        public void SaveNew()
        {
            Save();
            Clear();
        }

        public void SaveClose()
        {
            Save();
            if (UnitId==0)
            {
                _events.PublishOnUIThread(false);

            }
        }

        public void Clear()
        {
            UnitName = null;
            Enabled = false;

        }

        #endregion
        public void Save()
        {
            if (UnitId != 0)
            {
                var update = _unitOfWork.Units.GetById(UnitId);
                var test = CheckByName();
                if (update.Name != UnitName && test is true)
                {
                    Message("Name exist");
                    return;
                }
                else
                {
                    update.IsActive = Enabled;
                    update.CreatedAt = DateTime.Now;
                    update.Name = UnitName;
                    _unitOfWork.Units.Update(update);
                    _unitOfWork.Commit();
                    _events.PublishOnUIThread(false);
                }
            }
            else if (!CheckByName())
            {
                Data.Entities.Unit unit = new Data.Entities.Unit()
                {
                    Name = UnitName,
                    CreatedAt = DateTime.Now,
                    IsActive = Enabled
                };
                _unitOfWork.Units.Add(unit);
                _unitOfWork.Commit();
                _events.PublishOnUIThread("ret");
            }
        }

        private bool CheckByName()
        {
            return _unitOfWork.Units.Any(x => x.Name == _unitName);
        }

        public void Update(int id)
        {
            var getUnit = _unitOfWork.Units.GetById(id);
            UnitName = getUnit.Name;
            Enabled = getUnit.IsActive;
            Title = "Update";
        }
    }

}