﻿using Caliburn.Micro;
using Saodat.Data.Repository;
using Saodat.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using Saodat.Data.Concrete;
using Saodat.ViewModels.Base;

namespace Saodat.ViewModels.Unit
{
    public class UnitCollectionViewModel : ConductorBase, ICollection, IHandle<string>,IHandle<bool>
    {

        private readonly IEventAggregator _events;
        private readonly SimpleContainer _container;

        private readonly IUnitOfWork _unitOfWork;

        public int order = 1;
        private ObservableCollection<UnitModel> _data;
        public ObservableCollection<UnitModel> Data
        {
            get { return _data; }
            set
            {
                _data = value;
                NotifyOfPropertyChange(() => Data);
            }
        }


        private bool _isOpenDialog = false;
        public bool IsOpenDialog
        {
            get { return _isOpenDialog; }
            set
            {
                _isOpenDialog = value;
                NotifyOfPropertyChange(() => IsOpenDialog);
            }
        }

        public UnitCollectionViewModel(IEventAggregator events, SimpleContainer container, IUnitOfWork unitOfWork)
        {
            _container = container;
            _unitOfWork = unitOfWork;
            _events = events;
            _events.Subscribe(this);
            PageSize = new List<int>() { 10, 20, 30 };
            SelectedPageSize = PageSize[0];
            GetList(_keyword);
        }

        #region Buttons
        public void NewUnit()
        {
            
            IsOpenDialog = !IsOpenDialog;
            ActivateItem(_container.GetInstance<UnitViewModel>());
        }
        #endregion

        #region Properties

        private List<int> _pageSize;

        public List<int> PageSize
        {
            get { return _pageSize; }
            set
            {
                _pageSize = value;
                NotifyOfPropertyChange(() => PageSize);

            }
        }
        private int _page = 1;

        public int Page
        {
            get { return _page; }
            set
            {
                _page = value;
                NotifyOfPropertyChange(() => Page);
                NotifyOfPropertyChange(() => CanNextPage);
                NotifyOfPropertyChange(() => CanPreviousPage);
            }
        }

        private int _selectedPageSize = 20;

        public int SelectedPageSize
        {
            get { return _selectedPageSize; }
            set
            {
                _selectedPageSize = value;
                NotifyOfPropertyChange(() => SelectedPageSize);
                GetList(_keyword);
            }
        }

        private int _pageCount;

        public int PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;
                NotifyOfPropertyChange(() => PageCount);
                
            }
        }

        //seach Keyword
        private string _keyword;

        public string Keyword
        {
            get { return _keyword; }
            set
            {
                _keyword = value;
                NotifyOfPropertyChange(() => Keyword);
                GetList(_keyword);
            }
        }

        //seach Keyword
        private bool _enabled;

        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                _enabled = value;
                NotifyOfPropertyChange(() => Enabled);
                if (_selectedItem != null)
                { //Data.Entities.Product product = new Data.Entities.Product();
                    var exist = _unitOfWork.Units.GetById(_selectedItem.Id);
                    if (exist != null)
                    {
                        exist.IsActive = _enabled;
                        _unitOfWork.Units.Update(exist);
                        _unitOfWork.Commit();
                    }
                }
            }
        }

        private UnitModel _selectedItem;
        public UnitModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                NotifyOfPropertyChange(() => SelectedItem);

            }
        }

        public bool CanNextPage
        {
            get
            {
                if (Page < PageCount)
                {
                    return true;
                }
                return false;
            }
        }

        public bool CanPreviousPage
        {
            get
            {
                if (Page > 1)
                {
                    return true;
                }
                return false;
            }
        }

        private string _totalUnits;

        public string TotalUnits
        {
            get { return _totalUnits; }
            set
            {
                _totalUnits = value;
                NotifyOfPropertyChange(() => TotalUnits);
            }
        }
        #endregion


        public void GetList(string keyword = null)
        {
            order = 1;
            List<UnitModel> list = new List<UnitModel>();
            var query = keyword != null ? _unitOfWork.Units.All().Where(p => p.Name.Contains(keyword)).ToList() : _unitOfWork.Units.All().ToList();

            //total Units
            TotalUnits = _unitOfWork.Units.All().Count().ToString();
            PageCount = (int)Math.Ceiling(query.Count() / (double)SelectedPageSize);
            // Get by page
            int skip = SelectedPageSize * (Page - 1);
            var entities = query.Skip(skip).Take(SelectedPageSize).ToList();
            foreach (var item in entities)
            {
                list.Add(new UnitModel
                {
                    Id = item.Id,
                    Title = item.Name,
                    IsActive = item.IsActive,
                    Order = order
                });
                order++;
            }
            Data = new BindableCollection<UnitModel>(list);


        }

        public void Update()
        {
            var test = SelectedItem;
        }

        public void NextPage()
        {
            Page++;
            GetList(null);
        }
        public void PreviousPage()
        {
            Page--;
            GetList(null);
        }

        public void Handle(bool message)
        {
            IsOpenDialog = message;
            GetList(null);
        }

        public void Handle(string message)
        {
            GetList(null);
        }

        public void Edit()
        {
            IsOpenDialog = !IsOpenDialog;
            var update = _container.GetInstance<UnitViewModel>();
            update.UnitId = SelectedItem.Id;
            ActivateItem(update);
        }
    }
}