﻿using Caliburn.Micro;
using Saodat.Data.Concrete;
using Saodat.Models;
using Saodat.Services;
using Saodat.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.ViewModels.Report
{
    public class ReportViewModel : ConductorBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly SimpleContainer _container;
        private readonly IProductReportService report;
        public int Order = 1;
        public ReportViewModel(IUnitOfWork unitOfWork, SimpleContainer container, IProductReportService report)
        {
            _unitOfWork = unitOfWork;
            _container = container;
            this.report = report;
            var ExpTypes = new List<GroupModel>()
            {
                new GroupModel(){Id = 1, Title = "Експедитор"},
                new GroupModel(){Id = 2, Title = "Супермаркет"}
            };
            Types = new List<GroupModel>(ExpTypes);
            SelectedType = Types[0];
            End = DateTime.Now.Date;
            Start = End.AddDays(-30);

        }

        private ObservableCollection<ReportListModel> _data;
        public ObservableCollection<ReportListModel> Data
        {
            get => _data;
            set
            {
                _data = value;
                NotifyOfPropertyChange(() => Data);
            }
        }

        private DateTime _start;
        public DateTime Start
        {
            get { return _start; }
            set
            {
                if (value <= End)
                {
                    _start = value;
                    Show();
                }
                NotifyOfPropertyChange(() => Start);
            }
        }

        private DateTime _end;
        public DateTime End
        {
            get { return _end; }
            set
            {
                if (value >= Start)
                {
                    _end = value;
                    Show();
                }
                NotifyOfPropertyChange(() => End);
            }
        }

        private GroupModel _selectedType;
        public GroupModel SelectedType
        {
            get => _selectedType;
            set
            {
                _selectedType = value;
                NotifyOfPropertyChange(() => SelectedType);
                Show();

            }
        }


        public void Show()
        {
            if (Start <= End)
            {
                if (SelectedType.Id == 2)
                {
                    GetStores();
                }
                else
                {
                    GetData();
                }
            }

        }

        private void GetStores()
        {
            var query = (from p in _unitOfWork.Stores.GetDbSet().Include(x => x.Expeditor).Where(x => x.IsActive == true).ToList()
                         join rp in _unitOfWork.ReturnProducts.All().Where(x => DbFunctions.TruncateTime(x.CreatedAt) >= Start.Date && DbFunctions.TruncateTime(x.CreatedAt) <= End.Date).ToList()
                        on p.ExpeditorId equals rp.ConsumerId into grouping
                         select new ReportListModel
                         {
                             Name = $"{p.Name} ({p.Expeditor.Surname} {p.Expeditor.Name.Substring(0, 1).ToUpper()})",
                             Returned = grouping.Sum(x => x.ReturnedAmount),
                             Given = grouping.Sum(x => x.GivenAmount)
                         }).ToList();
            int cnt = 1;
            for (int i = 0; i < query.Count; i++)
            {
                query[i].Order = cnt;
                cnt++;
            }
            Data = new BindableCollection<ReportListModel>(query);
        }

        private List<GroupModel> _types = null;
        public List<GroupModel> Types
        {
            get => _types;
            set
            {
                _types = value;
                NotifyOfPropertyChange(() => Types);
            }
        }

        private void GetData()
        {
            var query = (from p in _unitOfWork.Expeditors.GetDbSet().Include(x => x.Stores).Where(x => x.IsActive == true && x.Stores.Count == 0).ToList()
                         join rp in _unitOfWork.ReturnProducts.All().Where(x => DbFunctions.TruncateTime(x.CreatedAt) >= Start.Date && DbFunctions.TruncateTime(x.CreatedAt) <= End.Date).ToList()
                         on p.Id equals rp.ConsumerId into grouping
                         select new ReportListModel
                         {
                             Name = p.Name,
                             Returned = grouping.Sum(x => x.ReturnedAmount),
                             Given = grouping.Sum(x => x.GivenAmount)
                         }).ToList();
            int cnt = 1;
            for (int i = 0; i < query.Count; i++)
            {
                query[i].Order = cnt;
                cnt++;
            }
            Data = new BindableCollection<ReportListModel>(query);
        }

        private int _saveProcess;

        public int SaveProcess
        {
            get { return _saveProcess; }
            set
            {
                _saveProcess = value;
                NotifyOfPropertyChange(() => SaveProcess);
            }
        }



        public void Print()
        {
            SaveProcess = 100;
            var expeditor = SelectedType.Id == 1 ? true : false;
            report.ExportToExcel(Start, End, expeditor);
        }


    }
}
