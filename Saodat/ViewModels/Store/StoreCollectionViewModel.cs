﻿using Caliburn.Micro;
using Saodat.Data.Repository;
using Saodat.Models;
using Saodat.ViewModels.Store;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using Saodat.Data.Concrete;
using Saodat.Data.Entities;
using Saodat.ViewModels.Base;

namespace Saodat.ViewModels.Store
{
    public class StoreCollectionViewModel : ConductorBase,ICollection, IHandle<string>,IHandle<bool>
    {

        private readonly IEventAggregator _events;
        private readonly SimpleContainer _container;
        private readonly IUnitOfWork _unitOfWork;
        public int order = 1;
        private ObservableCollection<StoreModel> _data;
        public ObservableCollection<StoreModel> Data
        {
            get => _data;
            set
            {
                _data = value;
                NotifyOfPropertyChange(() => Data);
            }
        }


        private bool _isOpenDialog = false;
        public bool IsOpenDialog
        {
            get => _isOpenDialog;
            set { _isOpenDialog = value; NotifyOfPropertyChange(() => IsOpenDialog); }
        }

        public StoreCollectionViewModel(IEventAggregator events, SimpleContainer container, IUnitOfWork unitOfWork)
        {
            _container = container;
            _unitOfWork = unitOfWork;
            _events = events;
            _events.Subscribe(this);
            PageSize = new List<int>() { 10, 20, 30 };
            SelectedPageSize = PageSize[0];
            GetList(null);
        }

        #region Buttons
        public void NewStore()
        {
            //_events.PublishOnUIThread("Store");
            IsOpenDialog = !IsOpenDialog;
            ActivateItem(_container.GetInstance<StoreViewModel>());
        }
        public void Edit()
        {
            IsOpenDialog = !IsOpenDialog;
            var update = _container.GetInstance<StoreViewModel>();
            update.StoreId = Selecteditem.Id;
            ActivateItem(update);
        }
        #endregion

        #region Properties

        private List<int> _pageSize;

        public List<int> PageSize
        {
            get => _pageSize;
            set
            {
                _pageSize = value;
                NotifyOfPropertyChange(() => PageSize);

            }
        }
        private int _page = 1;

        public int Page
        {
            get => _page;
            set
            {
                _page = value;
                NotifyOfPropertyChange(() => Page);
                NotifyOfPropertyChange(() => CanNextPage);
                NotifyOfPropertyChange(() => CanPreviousPage);
            }
        }

        private int _selectedPageSize = 20;

        public int SelectedPageSize
        {
            get => _selectedPageSize;
            set
            {
                _selectedPageSize = value;
                NotifyOfPropertyChange(() => SelectedPageSize);
               
            }
        }

        private int _pageCount;

        public int PageCount
        {
            get => _pageCount;
            set
            {
                _pageCount = value;
                NotifyOfPropertyChange(() => PageCount);

            }
        }

        //seach Keyword
        private string _keyword;

        public string Keyword
        {
            get => _keyword;
            set
            {
                _keyword = value;
                NotifyOfPropertyChange(() => Keyword);
                GetList(_keyword);
            }
        }

        //seach Keyword
        private bool _enabled;

        public bool Enabled
        {
            get => _enabled;
            set
            {
                _enabled = value;
                NotifyOfPropertyChange(() => Enabled);
                if (_selecteditem != null)
                { //Data.Entities.Store Store = new Data.Entities.Store();
                    var exist = _unitOfWork.Stores.GetById(_selecteditem.Id);
                    if (exist != null)
                    {
                        exist.IsActive = _enabled;
                        _unitOfWork.Stores.Update(exist);
                    }
                }
            }
        }

        private StoreModel _selecteditem;
        public StoreModel Selecteditem
        {
            get => _selecteditem;
            set
            {
                _selecteditem = value;
                NotifyOfPropertyChange(() => Selecteditem);

            }
        }

        public bool CanNextPage
        {
            get
            {
                if (Page < _pageCount)
                {
                    return true;
                }
                return false;
            }
        }

        public bool CanPreviousPage
        {
            get
            {
                if (Page > 1)
                {
                    return true;
                }
                return false;
            }
        }

        private string _totalStores;

        public string TotalStores
        {
            get => _totalStores;
            set
            {
                _totalStores = value;
                NotifyOfPropertyChange(() => TotalStores);
            }
        }
        #endregion

        public void GetList(string keyword)
        {
            order = 1;
            List<StoreModel> list = new List<StoreModel>();
            var query = keyword != null ? _unitOfWork.Stores.All().Include(x => x.Expeditor).Where(p => p.Name.Contains(keyword)).ToList() : _unitOfWork.Stores.All().Include(x => x.Expeditor).ToList();
            //total products
            TotalStores = _unitOfWork.Stores.All().Count().ToString();
            PageCount = (int)Math.Ceiling(query.Count() / (double)_selectedPageSize);
            // Get by page
            int skip = _selectedPageSize * (Page - 1);
            var entities = query.Skip(skip).Take(_selectedPageSize).ToList();
            foreach (var item in entities)
            {
                list.Add(new StoreModel
                {
                    Id = item.Id,
                    Order = order,
                    Name = item.Name,
                    Expeditor = item.Expeditor.Name,
                    StoreType = item.StoreTypeId.ToString(),
                    IsActive = item.IsActive
                });
                order++;
            }
            Data = new BindableCollection<StoreModel>(list);
        }
        public void NextPage()
        {
            Page++;
            GetList(null);
        }
        public void PreviousPage()
        {
            Page--;
            GetList(null);
        }

        public void Handle(bool message)
        {
            IsOpenDialog = message;
            GetList(null);
        }

        public void Handle(string message)
        {
           
            if (message == "DialogClose") IsOpenDialog = !IsOpenDialog;
            else GetList(null);
        }
        

    }
}