﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using AutoMapper;
using Caliburn.Micro;
using MaterialDesignThemes.Wpf;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;
using Saodat.Data.Concrete;
using Saodat.Data.Entities;
using Saodat.Data.Repository;
using Saodat.Models;
using Saodat.ViewModels.Base;
using Saodat.ViewModels.Store;

namespace Saodat.ViewModels.Store
{
    public class StoreViewModel : ScreenBase, IViewModel
    {


        private readonly IEventAggregator _events;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public StoreViewModel(IEventAggregator events, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _events = events;
            _unitOfWork = unitOfWork;
            _mapper = mapper;

            var storeType = _unitOfWork.StoreTypes.All().Where(x => x.IsActive == true).ToList();
            StoreType = new List<StoreTypeModel>(_mapper.Map<List<StoreTypeModel>>(storeType));

            var expeditor = _unitOfWork.Expeditors.GetAll().Where(x => x.IsActive == true).Select(s => new ExpeditorListModel()
            {
                Id = s.Id,
                Name = s.Surname + " " + s.Name + " " + s.LastName
            });

            Expeditors = new List<ExpeditorListModel>(expeditor);
        }

        #region Properties

        private int _storeId;

        public int StoreId
        {
            get => _storeId;
            set
            {
                _storeId = value;
                NotifyOfPropertyChange(() => StoreId);
                Update(StoreId);
            }
        }

        public void Update(int id)
        {
            var getStore = _unitOfWork.Stores.GetById(id);
            StoreName = getStore.Name;
            Address = getStore.Address;
            PhoneNumber = getStore.PhoneNumber;
            Enabled = getStore.IsActive;
            Manager = getStore.ManagerName;
            SelectedStoreType = StoreType.Where(x => x.Id == (int)getStore.StoreTypeId).First();
            SelectedExpeditor = Expeditors.Where(x => x.Id == getStore.ExpeditorId).First();
            Title = "Update";
        }

        private string _storeName;
        public string StoreName
        {
            get => _storeName;
            set
            {
                _storeName = value;
                NotifyOfPropertyChange(() => StoreName);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private string _address;
        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                NotifyOfPropertyChange(() => Address);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private string _manager;
        public string Manager
        {
            get => _manager;
            set
            {
                _manager = value;
                NotifyOfPropertyChange(() => Manager);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get => _phoneNumber;
            set
            {
                _phoneNumber = value;
                NotifyOfPropertyChange(() => PhoneNumber);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private ObservableCollection<ErrorViewModel> _errors;
        public ObservableCollection<ErrorViewModel> Errors
        {
            get => _errors;
            set
            {
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }

        private List<StoreTypeModel> _storeType;
        public List<StoreTypeModel> StoreType
        {
            get => _storeType;
            set
            {
                _storeType = value;
                NotifyOfPropertyChange(() => StoreType);
            }
        }

        private StoreTypeModel _selectedStoreType;
        public StoreTypeModel SelectedStoreType
        {
            get => _selectedStoreType;
            set
            {
                _selectedStoreType = value;
                NotifyOfPropertyChange(() => SelectedStoreType);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private List<ExpeditorListModel> _expeditors;
        public List<ExpeditorListModel> Expeditors
        {
            get => _expeditors;
            set
            {
                _expeditors = value;
                NotifyOfPropertyChange(() => Expeditors);
            }
        }

        private ExpeditorListModel _selectedExpeditor;
        public ExpeditorListModel SelectedExpeditor
        {
            get => _selectedExpeditor;
            set
            {
                _selectedExpeditor = value;
                NotifyOfPropertyChange(() => SelectedExpeditor);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }


        private bool _enabled;
        public bool Enabled
        {
            get => _enabled;
            set
            {
                _enabled = value;
                NotifyOfPropertyChange(() => Enabled);
            }
        }

        public bool CanSaveNew
        {
            get
            {
                if (StoreId != 0)
                {
                    return false;
                }
                return StoreName != null &&
                    PhoneNumber != null &&
                    Manager != null &&
                    Address != null &&
                    SelectedExpeditor != null &&
                    SelectedStoreType != null;
            }
        }

        public bool CanSaveClose =>
            StoreName != null &&
            PhoneNumber != null &&
            Manager != null &&
            Address != null &&
            SelectedExpeditor != null &&
            SelectedStoreType != null;
        #endregion

        #region Buttons


        public void Cancel()
        {

        }
        public void SaveNew()
        {
            Save();
            Clear();
        }

        public void SaveClose()
        {
            Save();
            if (StoreId == 0)
            {
                _events.PublishOnUIThread(false);
            }

        }

        public void Clear()
        {
            StoreName = null;
            Address = null;
            Manager = null;
            PhoneNumber = null;
        }

        #endregion
        public void Save()
        {
            if (StoreId != 0)
            {
                var update = _unitOfWork.Stores.GetById(StoreId);
                var test = CheckByName();
                if (update.Name != StoreName && test is true)
                {
                    Message("Name exist");
                    return;
                }
                else
                {
                    update.Name = StoreName;
                    update.PhoneNumber = PhoneNumber;
                    update.StoreTypeId = SelectedStoreType.Id;
                    update.IsActive = Enabled;
                    update.Address = Address;
                    update.ExpeditorId = SelectedExpeditor.Id;
                    update.ManagerName = Manager;
                    _unitOfWork.Stores.Update(update);
                    _unitOfWork.Commit();
                    _events.PublishOnUIThread(false);
                }
            }
            else if (!CheckByName())
            {
                Data.Entities.Store store = new Data.Entities.Store()
                {
                    Name = StoreName,
                    Address = Address,
                    IsActive = Enabled,
                    ExpeditorId = SelectedExpeditor.Id,
                    ManagerName = Manager,
                    StoreTypeId = SelectedStoreType.Id,
                    PhoneNumber = PhoneNumber,
                };
                _unitOfWork.Stores.Add(store);
                _unitOfWork.Commit();
                return;
            }
            Message("Name exists please check a unique name");

        }

        public bool CheckByName()
        {
            return _unitOfWork.Stores.Any(x => x.Name == StoreName);

        }




    }
}