﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saodat.ViewModels.Base
{
    interface IViewModel
    {
        void Cancel();
        void SaveNew();
        void SaveClose();
        void Clear();
        void Save();
        bool CanSaveNew { get; }
        bool CanSaveClose { get; }
        void Update(int id);
        void Message(string message);
        
    }
}
