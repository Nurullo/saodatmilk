﻿using Caliburn.Micro;
using MaterialDesignThemes.Wpf;
using System.ComponentModel;
namespace Saodat.ViewModels.Base
{
    public abstract class ScreenBase : Screen
    {
        public SnackbarMessageQueue SnackBarMessage { get; } = new SnackbarMessageQueue();

        private string _title = "Save and Close";

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                NotifyOfPropertyChange(() => Title);
            }
        }


        public void Message(string message)
        {
            SnackBarMessage.Enqueue(message);
        }
    }
}