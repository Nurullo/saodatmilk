﻿using Caliburn.Micro;
using Saodat.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Saodat.Data.Concrete;
using Saodat.ViewModels.Base;

namespace Saodat.ViewModels.ProductCategory
{
    class ProductCategoryCollectionViewModel : ConductorBase,ICollection,IHandle<string>,IHandle<bool>
    {
        private readonly IEventAggregator _events;
        private readonly SimpleContainer _container;
        private readonly IUnitOfWork _unitOfWork;
       public int order = 1;
        private ObservableCollection<ProductCategoryModel> _data;
        public ObservableCollection<ProductCategoryModel> Data
        {
            get { return _data; }
            set
            {
                _data = value;
                NotifyOfPropertyChange(() => Data);
            }
        }

        private bool _isOpenDialog = false;
        public bool IsOpenDialog
        {
            get { return _isOpenDialog; }
            set { _isOpenDialog = value; NotifyOfPropertyChange(() => IsOpenDialog); }
        }

        public ProductCategoryCollectionViewModel(IEventAggregator events, SimpleContainer container, IUnitOfWork unitOfWork)
        {
            _container = container;
            _unitOfWork = unitOfWork; 
              _events = events;
          _events.Subscribe(this);
              PageSize = new List<int>() { 10, 20, 30 };
              SelectedPageSize = PageSize[0];
            GetList(null);

        }

        #region Buttons
        public void CreateCategory()
        {
            _events.Subscribe(this);
            //_events.PublishOnUIThread("Product");
            IsOpenDialog = !IsOpenDialog;
            ActivateItem(_container.GetInstance<ProductCategoryViewModel>());
        }
        #endregion

        #region Properties
        private List<int> _pageSize;
        public List<int> PageSize
        {
            get { return _pageSize; }
            set
            {
                _pageSize = value;
                NotifyOfPropertyChange(() => PageSize);
            }
        }
        private int _page = 1;

        public int Page
        {
            get { return _page; }
            set
            {
                _page = value;
                NotifyOfPropertyChange(() => Page);
                NotifyOfPropertyChange(() => CanNextPage);
                NotifyOfPropertyChange(() => CanPreviousPage);
            }
        }
        private int _selectedPageSize = 20;

        public int SelectedPageSize
        {
            get { return _selectedPageSize; }
            set
            {
                _selectedPageSize = value;
                NotifyOfPropertyChange(() => SelectedPageSize);
                GetList();
            }
        }

        private int _pageCount;

        public int PageCount
        {
            get => _pageCount;
            set
            {
                _pageCount = value;
                NotifyOfPropertyChange(() => PageCount);

            }
        }

        //seach Keyword
        private string _keyword;

        public string Keyword
        {
            get => _keyword;
            set
            {
                _keyword = value;
                NotifyOfPropertyChange(() => Keyword);
                GetList(_keyword);
            }
        }

        //seach Keyword
        private bool _isActive;

        public bool IsActive
        {
            get => _isActive;
            set
            {
                _isActive = value;
                NotifyOfPropertyChange(() => IsActive);
                //if (_selecteditem != null)
                //{ //Data.Entities.Product product = new Data.Entities.Product();
                //    var exist = _repository.GetById(_selecteditem.Id);
                //    if (exist != null)
                //    {
                //        exist.IsActive = _enabled;
                //        _repository.Update(exist);
                //        _repository.Commit();
                //    }
                //}
            }
        }

        private ProductCategoryModel _selecteditem = null;
        public ProductCategoryModel SelectedItem
        {
            get => _selecteditem;
            set
            {
                _selecteditem = value;
                NotifyOfPropertyChange(() => SelectedItem);

            }
        }

        public bool CanNextPage
        {
            get
            {
                if (Page < _pageCount)
                {
                    return true;
                }
                return false;
            }
        }

        public bool CanPreviousPage
        {
            get
            {
                if (Page > 1)
                {
                    return true;
                }
                return false;
            }
        }

        private string _totalCategory;

        public string TotalCategories
        {
            get { return _totalCategory; }
            set
            {
                _totalCategory = value;
                NotifyOfPropertyChange(() => TotalCategories);
            }
        }
        #endregion

        public void GetList(string keyword = null)
        {
            order = 1;
            List<ProductCategoryModel> list = new List<ProductCategoryModel>();
            var query = keyword != null ? _unitOfWork.ProductCategories.All().Where(p => p.Name.Contains(keyword)).ToList() : _unitOfWork.ProductCategories.All().ToList();

            //total products
            TotalCategories = _unitOfWork.Products.All().Count().ToString();
            PageCount = (int)Math.Ceiling(query.Count() / (double)_selectedPageSize);
            // Get by page
            int skip = _selectedPageSize * (Page - 1);
            var entities = query.Skip(skip).Take(_selectedPageSize).ToList();
            foreach (var item in entities)
            {
                list.Add(new ProductCategoryModel
                {
                    Id = item.Id,
                    IsActive =  item.IsActive,
                    Title = item.Name,
                    Order = order
                });
                order++;
            }
            Data = new BindableCollection<ProductCategoryModel>(list);
        }

       

        public void NextPage()
        {
            Page++;
            GetList(null);
        }
        public void PreviousPage()
        {
            Page--;
            GetList(null);
        }

        public void Handle(bool message)
        {
            IsOpenDialog = message;
            GetList(null);
        }

        //public void Print()
        //{
        //    //_excelPackage.Workbook.Worksheets.Add("New");
        //    //FileInfo exelInfo = new FileInfo(@"C:\Users\SuNu\Desktop\test.xlsx");
        //    //_excelPackage.SaveAs(exelInfo);
        //}



        public void Handle(string message)
        {
            GetList();
        }

        public void Edit()
        {
            IsOpenDialog = !IsOpenDialog;
            var update = _container.GetInstance<ProductCategoryViewModel>();
            update.ProductCategoryId = SelectedItem.Id;
            ActivateItem(update);
        }
    }
}
