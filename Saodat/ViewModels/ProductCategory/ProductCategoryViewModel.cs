﻿using Caliburn.Micro;
using Saodat.Data.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using Saodat.Data.Concrete;
using Saodat.Models;
using Saodat.ViewModels.Base;

namespace Saodat.ViewModels.ProductCategory
{
    public class ProductCategoryViewModel : ScreenBase, IViewModel
    {
        private readonly IEventAggregator _events;
        private readonly IUnitOfWork _unitOfWork;


        public ProductCategoryViewModel(IEventAggregator events, IUnitOfWork unitOfWork)
        {
            _events = events;
            _unitOfWork = unitOfWork;

        }
        #region Properties
        private int _productCategoryId = 0;
        public int ProductCategoryId
        {
            get => _productCategoryId;
            set
            {
                _productCategoryId = value;
                NotifyOfPropertyChange(() => ProductCategoryId);
                Update(_productCategoryId);
            }
        }
        private string _productCategoryName;

        public string ProductCategoryName
        {
            get => _productCategoryName;
            set
            {
                _productCategoryName = value;
                NotifyOfPropertyChange(() => ProductCategoryName);
                NotifyOfPropertyChange(() => CanSaveNew);
                NotifyOfPropertyChange(() => CanSaveClose);
            }
        }



        private ObservableCollection<ErrorViewModel> _errors;
        public ObservableCollection<ErrorViewModel> Errors
        {
            get => _errors;
            set
            {
                _errors = value;
                NotifyOfPropertyChange(() => Errors);
            }
        }


        private bool _enabled;
        public bool Enabled
        {
            get => _enabled;
            set
            {
                _enabled = value;
                NotifyOfPropertyChange(() => Enabled);
            }
        }

        public bool CanSaveNew
        {
            get
            {
                if (ProductCategoryId!=0)
                {
                    return false;
                }
                return !string.IsNullOrWhiteSpace(ProductCategoryName);
            }
        }

        public bool CanSaveClose => !string.IsNullOrWhiteSpace(ProductCategoryName);

        #endregion

        #region Buttons

        public void ErrorControl(string element, string description)
        {
            var error = new ErrorViewModel();
            error.Order++;
            error.Description = description;
            error.Element = element;

            Errors.Add(error);
        }



        public void Cancel()
        {

        }

        public void SaveNew()
        {
            Save();
            Clear();
        }

        public void SaveClose()
        {
            Save();
            if (ProductCategoryId==0)
            {
                _events.PublishOnUIThread(false);

            }
        }

        public void Clear()
        {
            ProductCategoryName = null;
            Enabled = false;

        }

        #endregion
        public void Save()
        {
            if (ProductCategoryId != 0)
            {
                var update = _unitOfWork.ProductCategories.GetById(ProductCategoryId);
                var test = CheckByName();
                if (update.Name != ProductCategoryName && test is true)
                {
                    Message("Name exist");
                    return;
                }
                else
                {
                    update.Name = ProductCategoryName;
                    update.CreatedAt = DateTime.Now;
                    update.IsActive = Enabled;
                    _unitOfWork.ProductCategories.Update(update);
                    _unitOfWork.Commit();
                    _events.PublishOnUIThread(false);
                }
            }
            else if (CheckByName())
            {
                Data.Entities.ProductCategory category = new Data.Entities.ProductCategory()
                {
                    Name = ProductCategoryName,
                    CreatedAt = DateTime.Now,
                    IsActive = Enabled
                };
                _unitOfWork.ProductCategories.Add(category);
                _unitOfWork.Commit();
                _events.PublishOnUIThread("refresh");
            }
            //Message("Name exists please check a unique name");
        }
        
        public bool CheckByName()
        {
            return _unitOfWork.ProductCategories.Any(x => x.Name == _productCategoryName);
            
        }

        public void Update(int id)
        {
            var getCategory = _unitOfWork.ProductCategories.GetById(id);
            ProductCategoryName = getCategory.Name;
            Enabled = getCategory.IsActive; 
            Title = "Update";
        }


    }
}
