﻿using Caliburn.Micro;
using Saodat.Data.Entities;
using Saodat.Data.Repository;
using Saodat.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Saodat.Data.Concrete;
using Saodat.Helpers;
using Saodat.Services;
using Saodat.ViewModels.Base;

namespace Saodat.ViewModels.Product
{
    public class ProductCollectionViewModel : ConductorBase, ICollection, IHandle<bool>, IHandle<string>
    {

        private readonly IEventAggregator _events;
        private readonly SimpleContainer _container;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProductReportService _productReportService;
        private int order = 1;
        private ObservableCollection<ProductModel> _data;
        public ObservableCollection<ProductModel> Data
        {
            get => _data;
            set
            {
                _data = value;
                NotifyOfPropertyChange(() => Data);
            }
        }


        private bool _isOpenDialog = false;
        public bool IsOpenDialog
        {
            get => _isOpenDialog;
            set
            {
                _isOpenDialog = value;
                NotifyOfPropertyChange(() => IsOpenDialog);
            }
        }

        #region constructor
        public ProductCollectionViewModel(IEventAggregator events, SimpleContainer container, IUnitOfWork unitOfWork, IProductReportService productReportService)
        {
            _container = container;
            _unitOfWork = unitOfWork;
            _productReportService = productReportService;
            _events = events;
            _events.Subscribe(this);
            PageSize = new List<int>() { 10, 20, 30 };
            SelectedPageSize = PageSize[0];
            GetList();
        }
        #endregion


        #region Buttons
        public void NewProduct()
        {
            _events.Subscribe(this);
            //_events.PublishOnUIThread("Product");
            IsOpenDialog = !IsOpenDialog;
            ActivateItem(_container.GetInstance<ProductViewModel>());
        }
        #endregion

        #region Properties

        private List<int> _pageSize;

        public List<int> PageSize
        {
            get => _pageSize;
            set
            {
                _pageSize = value;
                NotifyOfPropertyChange(() => PageSize);

            }
        }
        private int _page = 1;

        public int Page
        {
            get => _page;
            set
            {
                _page = value;
                NotifyOfPropertyChange(() => Page);
                NotifyOfPropertyChange(() => CanNextPage);
                NotifyOfPropertyChange(() => CanPreviousPage);
            }
        }

        private int _selectedPageSize = 10;

        public int SelectedPageSize
        {
            get => _selectedPageSize;
            set
            {
                _selectedPageSize = value;
                NotifyOfPropertyChange(() => SelectedPageSize);
                GetList(null);
            }
        }

        private int _pageCount;

        public int PageCount
        {
            get => _pageCount;
            set
            {
                _pageCount = value;
                NotifyOfPropertyChange(() => PageCount);

            }
        }

        //seach Keyword
        private string _keyword;
        public string Keyword
        {
            get => _keyword;
            set
            {
                _keyword = value;
                NotifyOfPropertyChange(() => Keyword);
                GetList(_keyword);
            }
        }

        //seach Keyword
        public bool _isActive = true;
        public bool IsActive
        {
            get => _isActive;
            set
            {
                _isActive = value;
                NotifyOfPropertyChange(() => IsActive);
                if (_selecteditem != null)
                { //Data.Entities.Product product = new Data.Entities.Product();
                    var exist = _unitOfWork.Products.GetById(_selecteditem.Id);
                    if (exist != null)
                    {
                        exist.Enabled = _isActive;
                        _unitOfWork.Products.Update(exist);
                        _unitOfWork.Commit();
                    }
                }
            }
        }

        private ProductModel _selecteditem;
        public ProductModel Selecteditem
        {
            get => _selecteditem;
            set
            {
                _selecteditem = value;
                NotifyOfPropertyChange(() => Selecteditem);

            }
        }

        public bool CanNextPage
        {
            get
            {
                if (Page < _pageCount)
                {
                    return true;
                }
                return false;
            }
        }

        public bool CanPreviousPage
        {
            get
            {
                if (Page > 1)
                {
                    return true;
                }
                return false;
            }
        }

        private string _totalProducts;

        public string TotalProducts
        {
            get => _totalProducts;
            set
            {
                _totalProducts = value;
                NotifyOfPropertyChange(() => TotalProducts);
            }
        }
        #endregion

        public void GetList(string keyword = null)
        {
            order = 1;
            List<ProductModel> list = new List<ProductModel>();
            var query = keyword != null ? _unitOfWork.Products.All().Include(s => s.Unit).Where(p => p.Name.Contains(keyword)).ToList() : _unitOfWork.Products.All().Include(s => s.Unit).ToList();

            //total products
            TotalProducts = _unitOfWork.Products.All().Count().ToString();
            PageCount = (int)Math.Ceiling(query.Count() / (double)_selectedPageSize);
            // Get by page
            int skip = _selectedPageSize * (Page - 1);
            var entities = query.Skip(skip).Take(_selectedPageSize).ToList();
            foreach (var item in entities)
            {
                list.Add(new ProductModel
                {
                    Enabled = item.Enabled,
                    FullTitle = item.Name + " " + item.Fatness.ToString("G29") + "% " + item.Value.ToString("G29") + item.Unit.Name,
                    Id = item.Id,
                    Order = order,
                    Unit = item.Unit.Name,
                    Price = item.Price.ToString("G29") + "сом"
                });
                order++;
            }
            Data = new BindableCollection<ProductModel>(list);
        }

        public void Handle(bool message)
        {
            IsOpenDialog = message;
            GetList(null);
        }

        public void Handle(string message)
        {
            GetList(_keyword);
        }

        public void Download()
        {

        }
        public void NextPage()
        {
            Page++;
            GetList(null);
        }
        public void PreviousPage()
        {
            Page--;
            GetList(null);
        }

        public void Edit()
        {
            IsOpenDialog = !IsOpenDialog;
            var update = _container.GetInstance<ProductViewModel>();
            update.ProductId = Selecteditem.Id;
            ActivateItem(update);
        }
    }

}
