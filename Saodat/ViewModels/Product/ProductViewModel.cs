﻿using Caliburn.Micro;
using Saodat.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Saodat.Data.Concrete;
using Saodat.ViewModels.Base;

namespace Saodat.ViewModels.Product
{
    public class ProductViewModel : ScreenBase, IViewModel
    {
        private readonly IEventAggregator _events;
        private readonly IUnitOfWork _unitOfWork;

        public ProductViewModel(IEventAggregator events, IUnitOfWork unitOfWork)
        {
            _events = events;
            _unitOfWork = unitOfWork;

            var units = _unitOfWork.Units.GetAll().Where(x => x.IsActive == true).Select(s => new UnitModel()
            {
                Id = s.Id,
                Title = s.Name
            });
            Units = new List<UnitModel>(units); ;

            var groups = _unitOfWork.ProductCategories.GetAll().Where(x => x.IsActive == true).Select(s => new GroupModel()
            {
                Id = s.Id,
                Title = s.Name
            });
            Categories = new List<GroupModel>(groups);
        }

        #region Properties

        private int _productId;
        public int ProductId
        {
            get => _productId;
            set
            {
                _productId = value;
                NotifyOfPropertyChange(() => ProductId);
                Update(ProductId);
            }
        }
        private string _productName;
        public string ProductName
        {
            get => _productName;
            set
            {
                _productName = value;
                NotifyOfPropertyChange(() => ProductName);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private decimal _fatness;

        public decimal Fatness
        {
            get => _fatness;
            set
            {
                _fatness = value;
                NotifyOfPropertyChange(() => Fatness);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private List<GroupModel> _categories;

        public List<GroupModel> Categories
        {
            get => _categories;
            set
            {
                _categories = value;
                NotifyOfPropertyChange(() => Categories);
                
            }
        }

        private GroupModel _selectedCategory;
        public GroupModel SelectedCategory
        {
            get => _selectedCategory;
            set
            {
                _selectedCategory = value;
                NotifyOfPropertyChange(() => SelectedCategory);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private List<UnitModel> _units;

        public List<UnitModel> Units
        {
            get => _units;
            set
            {
                _units = value;
                NotifyOfPropertyChange(() => Units);
            }
        }

        private UnitModel _selectedUnit;

        public UnitModel SelectedUnit
        {
            get => _selectedUnit;
            set
            {
                _selectedUnit = value;
                NotifyOfPropertyChange(() => SelectedUnit);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }

        }

        private decimal _price;

        public decimal Price
        {
            get => _price;
            set
            {
                _price = value;
                NotifyOfPropertyChange(() => Price);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        private decimal _value;

        public decimal Value
        {
            get => _value;
            set
            {
                _value = value;
                NotifyOfPropertyChange(() => Value);
                NotifyOfPropertyChange(() => CanSaveClose);
                NotifyOfPropertyChange(() => CanSaveNew);
            }
        }

        //search Keyword
        private bool _enabled;

        public bool Enabled
        {
            get => _enabled;
            set
            {
                _enabled = value;
                NotifyOfPropertyChange(() => Enabled);
            }
        }

        public bool CanSaveNew
        {
            get
            {
                if (ProductId != 0)
                {
                    return false;
                }
                return ProductName != null &&
                Price != 0 &&
                SelectedCategory != null &&
                SelectedUnit != null;
            }
        }

        public bool CanSaveClose
        {
            get
            {

                return ProductName != null &&
                Price != 0 &&
                SelectedCategory != null &&
                SelectedUnit != null;
            }
        }

        #endregion

        #region Buttons

        public void Cancel()
        {

        }

        public void SaveNew()
        {
            Save();
            Clear();
        }

        public void SaveClose()
        {
            Save();
            if (ProductId == 0)
            {
                _events.PublishOnUIThread(false);

            }
        }

        public void Clear()
        {
            ProductName = null;

            SelectedCategory = null;
            SelectedUnit = null;

        }

        #endregion
        public void Save()
        {
            if (ProductId != 0)
            {
                var update = _unitOfWork.Products.GetById(ProductId);
                var test = CheckByName();
                if (update.Name != ProductName && test is true)
                {
                    Message("Name exist");
                    return;
                }
                else
                {
                    update.Name = ProductName;
                    update.Fatness = Fatness;
                    update.CreatedAt = DateTime.Now;
                    update.Enabled = IsActive;
                    update.Price = Price;
                    update.Value = Value;
                    update.ProductCategoryId = SelectedCategory.Id;
                    update.UnitId = SelectedUnit.Id;
                    update.CreatedAt = DateTime.Now;
                    _unitOfWork.Products.Update(update);
                    _unitOfWork.Commit();
                    _events.PublishOnUIThread(false);
                }
            }
            else if (CheckByName() == false)
            {
                Data.Entities.Product product = new Data.Entities.Product()
                {
                    Enabled = _enabled,
                    CreatedAt = DateTime.Now,
                    Name = _productName,
                    Price = _price,
                    UnitId = SelectedUnit.Id,
                    Value = _value,
                    Fatness = Fatness,
                    ProductCategoryId = SelectedCategory.Id
                };
                _unitOfWork.Products.Add(product);
                _unitOfWork.Commit();
                _events.PublishOnUIThread("refresh");
            }
            // Message("Name exists please check a unique name");
        }

        public bool CheckByName()
        {
            return _unitOfWork.Products.Any(x => x.Name == _productName);
        }

        public void Update(int id)
        {
            var getProduct = _unitOfWork.Products.GetById(id);
            ProductName = getProduct.Name;
            Fatness = getProduct.Fatness;
            Enabled = getProduct.Enabled;
            Price = getProduct.Price;
            Value = getProduct.Value;
            
            SelectedUnit = Units.FirstOrDefault(x => x.Id == getProduct.UnitId);
            SelectedCategory = Categories.FirstOrDefault(x => x.Id == getProduct.ProductCategoryId);
            Title = "Update";
        }
    }
}