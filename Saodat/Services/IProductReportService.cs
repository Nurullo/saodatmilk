﻿using System;

namespace Saodat.Services
{
    public interface IProductReportService
    {
        void ExportToExcel(DateTime StartDate, DateTime EndDate, bool expeditor = true);
    }
}