﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using Saodat.Data.Concrete;
using Saodat.Data.Entities;
using Saodat.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Saodat.Services
{
    public class ProductReportService : IProductReportService
    {
        public ProductReportService()
        {

        }
        public void ExportToExcel(DateTime StartDate, DateTime EndDate, bool expeditor = true)
        {
            string title = "Имя Экспедитора";
            using (var dbContext = new SaodatDbContext())
            {
                using (var package = new ExcelPackage())
                {
                    int position = 0;
                    var ew = package.Workbook.Worksheets.Add("MySheet");

                    ew.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ew.Cells.Style.Font.Size = 16;
                    ew.Column(1).Width = 21;

                    position += 1;
                    ew.Cells["A" + position].Value = "Дата:\n" + StartDate.ToString("dd.MM.yyyy") + " до " + EndDate.ToString("dd.MM.yyyy");
                    ew.Cells["A" + position].Style.WrapText = true;
                    BorderStyle("A" + position, ew);
                    BorderStyle("A" + (position + 1), ew);
                    ew.Row(position).Height = 100;
                    ew.Row(position).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    if (!expeditor)
                    {
                        title = "Супермаркеты";
                    }
                    ew.Cells["A" + (position + 1)].Value = title;
                    ew.Cells["A" + (position + 1)].Style.WrapText = true;
                    BorderStyle("A" + (position + 1), ew);

                    var products = dbContext.Products.Where(x => x.Enabled).Include(x => x.Unit).ToList();
                    //get data and olso check for type of consumer
                    List<GroupReport> query = GetData(StartDate, EndDate, dbContext, expeditor);

                    char start = 'B';
                    for (int i = 0; i < products.Count; i++)
                    {
                        ew.Cells[start.ToString() + position + ":" + start.ToString() + (position + 1)].Merge = true;
                        ew.Cells[start.ToString() + position].Value = $"{products[i].Name} {products[i].Value.ToString("G29")}{products[i].Unit.Name} {products[i].Fatness.ToString("G29")}%";
                        ew.Cells[start.ToString() + position].Style.TextRotation = 90;
                        ew.Cells[start.ToString() + position].Style.WrapText = true;
                        BorderStyle(start.ToString() + position + ":" + start.ToString() + (position + 1), ew);
                        start++;
                    }

                    position += 2;
                    for (int i = 0; i < query.Count; i++)
                    {
                        ew.Cells["A" + position].Value = query[i].Name;
                        BorderStyle("A" + position, ew);

                        ew.Row(position).Height = 27;
                        start = 'B';

                        for (int j = 0; j < query[i].Data.Count; j++)
                        {
                            AddDiagonal(ew, start + position.ToString(), query[i].Data[j].ReturnedAmount == 0 ? "" : query[i].Data[j].ReturnedAmount.ToString(), query[i].Data[j].GivenAmount == 0 ? "" : query[i].Data[j].GivenAmount.ToString());
                            BorderStyle(start + position.ToString(), ew);

                            start++;
                        }
                        position++;
                    }

                    ew.Cells["A" + position].Value = "Хамаги";
                    BorderStyle("A" + position, ew);
                    ew.Cells["A" + (position + 1)].Value = "Буридан";
                    BorderStyle("A" + (position + 1), ew);

                    start = 'B';

                    var total = (from p in dbContext.Products.Where(x => x.Enabled == true).ToList()
                                 join rp in dbContext.ReturnProducts.
                                 Where(x => x.Consumer.Stores.Count == 0 && DbFunctions.TruncateTime(x.CreatedAt) >= StartDate && DbFunctions.TruncateTime(x.CreatedAt) <= EndDate).ToList().GroupBy(x => x.ProductId)
                                 on p.Id equals rp.Key into list
                                 from ll in list.DefaultIfEmpty()
                                 select new
                                 {
                                     returned = ll == null ? 0 : ll.Sum(x => x.ReturnedAmount),
                                     given = ll == null ? 0 : ll.Sum(x => x.GivenAmount)
                                 }
                                  ).ToList();
                    for (int i = 0; i < total.Count; i++)
                    {
                        ew.Cells[start + position.ToString()].Value = (total[i].returned);
                        BorderStyle(start + position.ToString(), ew);
                        ew.Cells[start + (position + 1).ToString()].Value = (total[i].given);
                        BorderStyle(start + (position + 1).ToString(), ew);
                        start++;
                    }

                    using (var fbd = new FolderBrowserDialog())
                    {
                        DialogResult result = fbd.ShowDialog();

                        if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                        {
                            var path = fbd.SelectedPath;
                            var name = "SaodatReport" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx";
                            FileInfo excelFile = new FileInfo(path + @"\" + name);
                            try
                            {
                                package.SaveAs(excelFile);
                                Process.Start(path + @"\" + name);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("File is opened please close the file");
                            }

                        }
                    }

                }
            }
        }

        private static List<GroupReport> GetData(DateTime StartDate, DateTime EndDate, SaodatDbContext dbContext, bool expeditor)
        {
            if (expeditor)
            {
                var mm = (from ex in dbContext.Expeditors.Where(x => x.IsActive == true && x.Stores.Count == 0).ToList()
                          select new GroupReport
                          {
                              Name = ex.Name,
                              Data = (from p in dbContext.Products.ToList()
                                      join rp in dbContext.ReturnProducts.Where(x => x.Product.Enabled == true && x.ConsumerId == ex.Id && DbFunctions.TruncateTime(x.CreatedAt) >= StartDate && DbFunctions.TruncateTime(x.CreatedAt) <= EndDate).ToList().GroupBy(x => x.ProductId)
                                      on p.Id equals rp.Key into list
                                      from ll in list.DefaultIfEmpty()
                                      select new RegisterList
                                      {
                                          Title = p.Name,
                                          ReturnedAmount = (ll == null ? 0 : ll.Sum(x => x.ReturnedAmount)),
                                          GivenAmount = (ll == null ? 0 : ll.Sum(x => x.GivenAmount)),
                                      }).ToList()

                          }).ToList();
                return mm;
            }
            else
            {
                var tt = (from ex in dbContext.Stores.Where(x => x.IsActive == true).Include(x => x.Expeditor).ToList()
                          select new GroupReport
                          {
                              Name = $"{ex.Name} ({ex.Expeditor.Surname} {ex.Expeditor.Name.Substring(0, 1)})",
                              Data = (from p in dbContext.Products.ToList()
                                      join rp in dbContext.ReturnProducts.Where(x => x.Product.Enabled == true && x.ConsumerId == ex.ExpeditorId && x.CreatedAt >= StartDate && x.CreatedAt <= EndDate).ToList().GroupBy(x => x.ProductId)
                                      on p.Id equals rp.Key into list
                                      from ll in list.DefaultIfEmpty()
                                      select new RegisterList
                                      {
                                          Title = p.Name,
                                          ReturnedAmount = (ll == null ? 0 : ll.Sum(x => x.ReturnedAmount)),
                                          GivenAmount = (ll == null ? 0 : ll.Sum(x => x.GivenAmount)),
                                      }).ToList()
                          }).ToList();

                return tt;
            }

        }

        private static void BorderStyle(string position, ExcelWorksheet ew)
        {
            ew.Cells[position].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            ew.Cells[position].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            ew.Cells[position].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            ew.Cells[position].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private static void AddDiagonal(ExcelWorksheet ws, string diagonalLocation, string returned, string given)
        {
            var diagonalCell = ws.Cells[diagonalLocation];
            var border = diagonalCell.Style.Border;
            border.Diagonal.Style = ExcelBorderStyle.Thin;
            border.DiagonalUp = true;

            diagonalCell.Style.Font.Size = 12;
            diagonalCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            diagonalCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            var altEnter = ((char)10).ToString();
            var spaces = " ";
            var diagonalText = string.Format("{2}{0}{1}{1}{1}{1}{1}{3}", altEnter, spaces, returned, given);
            diagonalCell.Value = diagonalText;
            diagonalCell.Style.WrapText = true;
        }
    }
}